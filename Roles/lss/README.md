Role Name
=========

lss : installs and configures the local server service.

Official OS Support
-------------------

The OS Support expected to work for this role

 - CentOS 7 
 - Debian 8

Role Variables
--------------

**Note** : The latest version will be installed by default. Only when you do NOT want the latest do you need to define a specific version.

| Variable                 | Default     | Description  |
| ------------------------ |------------ | ------------ |
| `lss_version`                | `None`      | Specific version to install, e.g `1.1.0.48`. |
| `csshostname`            | `127.0.0.1` | The hostname of the CSS to connect to.       |
| `loglevel`               | `min`       | `min, max, off, L1, L2, L3`. The logging level for LSS. |

Dependencies
------------

The following **MUST** be set in the playbook as vars :

| Variable           | Description  |
| ------------------ | ------------ |
|`repo_type`      | `Set as public` |
|`apt_section`  | `Type of repo. Typically this is set as "main".`|
|`yum_beta`   | `Should yum beta repository be enabled. Options are "no" or "yes"`|

Example Playbook
----------------

```
#!yaml
- name: Install LSS
  hosts: someserver
  remote_user: ansible
  become: yes
  vars:
    repo_type: public
    apt_section: "main"
    yum_beta: "no"
    csshostname: 10.0.0.11
  roles:
    - role: lss
```


Author Information
------------------

Support <support@incognito.com>
