Role Name
=========

incognito\_repository\_common: Manages installation of Incognito Common Package repositories, to facilitate installation of Incognito products.

Official OS Support
-------------------

- CentOS : 7
- Debian : 8

Role Variables
--------------

None

Dependencies
------------

The following **MUST** be set in the playbook as vars :

| Variable         | Description  |
| ---------------- | ------------ |
|`repo_type`      | `Set as public` |
|`apt_section`  | `Type of repo. Typically this is set as "main".`|
|`yum_beta`   | `Should yum beta repository be enabled. Options are "no" or "yes"`|

Example Playbook
----------------

```
#!yaml
- name: Setup of XYZ
  hosts: someserver
  remote_user: ansible
  become: yes
  vars:
    repo_type: public
    apt_section: "main"
    yum_beta: "no"
  roles:
     - role: incognito_repository_common
```

Author Information
------------------

Support <support@incognito.com>
