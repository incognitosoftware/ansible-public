Role Name
=========

nodesource :  Install nodejs (npm is installed at the same time) from nodesource

Official OS Support
-------------------

 - Debian 8

Role Variables
--------------

None

Dependencies
------------

None

Example Playbook
----------------

```
#!yaml
- name: Setup of XYZ
  hosts: someserver
  remote_user: ansible
  become: yes
  roles:
    - nodesource
```


Author Information
------------------

Support <support@incognito.com>
