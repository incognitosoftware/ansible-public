Role Name
=========

cwmp_install : Installs CWMP on the target server.

Official OS Support
-------------------
 - CentOS 7
 - Debian 8

Role Variables
--------------

| Variable                  | Default                 | Description  |
| ------------------------- |------------------------ | ------------ |
| `cwmp_version`            |  none                   | CWMP version can be specified, or will the latest will be installed (default) |
| `rabbitmq_port`           | `5672`                  | The RabbitMQ port.|
| `ums_port`                | `9995`                  | The UMS port.|
| `ums_host`                | `localhost`             | Value to be set for `UMSCORBALOC` in the CWMP configuration file. |
| `db_type`                 | `mysql`                 | The database can be set to 'postgres' or 'mysql' (default). |


Dependencies
------------

The following **MUST** be set in the playbook as vars :

| Variable              | Description  |
| --------------------- | ------------ |
| `repo_type`           | `Set as public` |
| `apt_section`    		  | Type of repo. Usually it's `"main"` for GA and `"development"` for latest. |
| `yum_beta`            | Should yum beta repository be enabled. Options are `"yes"` or `"no"`. |
| `cwmp_key`	          | License key for CWMP |
| `cwmp_major_ver`      | Major version of CWMP to be installed. |
| `rabbitmq_host`       | The RabbitMQ service address.|
| `rabbitmq_user`       | The RabbitMQ username.|
| `rabbitmq_password`   | The RabbitMQ password.|
| `multicast_region_id`	| `1-255`. Value to be set for multicast region ID. |



Example Playbook
----------------

```
#!yaml
- name: Install CWMP with Ansible
  hosts: someserver
  remote_user: ansible
  become: yes
  vars:
      repo_type: public
      apt_section: "main"
      yum_beta: "no"
      cwmp_key: 03X-CWM001-1111-1111-1111-1111
      cwmp_major_ver: 3
      rabbitmq_host: localhost
      rabbitmq_user: incognito
      rabbitmq_password: incognito
      multicast_region_id: 123
  roles:
      - role: cwmp
```

Author Information
------------------
Support <support@incognito.com>
