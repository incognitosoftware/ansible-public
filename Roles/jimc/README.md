Role Name
=========

jimc :  Installs the Incognito JIMC for BCC

Official OS Support
-------------------

 - CentOS 7
 - Debian 8

Role Variables
--------------

**Note** : The latest version will be installed by default. Only when you do NOT want the latest do you need to define a specific version.

| Variable                 | Default       | Description  |
| ------------------------ |-------------- | ------------ |
| `jimc_port`      | `8080`| `Port that JIMC will listen on` |
| `jimc_version`   | `None`| `Specific version to install e.g. 7.0.0.1 or if there are more than one revision of the same version e.g. 7.0.0.1-2)` |

Dependencies
------------

The following **MUST** be set in the playbook as vars :

| Variable         | Description  |
| ---------------- | ------------ |
|`repo_type`      | `Set as public` |
|`apt_section`  | `Type of repo. Typically this is set as "main".`|
|`yum_beta`   | `Should yum beta repository be enabled. Options are "no" or "yes"`|

Example Playbook
----------------

```
#!yaml
- name: Setup of XYZ
  hosts: someserver
  remote_user: ansible
  become: yes
  vars:
    repo_type: public
    apt_section: "main"
    yum_beta: "no"
  roles:
     - role: jimc
```


Author Information
------------------

Support <support@incognito.com>
