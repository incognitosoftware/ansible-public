unixodbc_postgresql
=========

Installs postgres-odbc and defines odbcinst.ini.

Official OS Support
------------

 - CentOS 7
 - Debian 8


Role Variables
--------------
	
None

Dependencies
------------

None


Example Playbook
----------------

```
#!yaml
- name: Install postgresql odbc connector 
  hosts: my_server
  remote_user: ansible
  become: yes
  roles:
    - unixodbc_postgresql
```


Author Information
------------------
Support <support@incognito.com>
