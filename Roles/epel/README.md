Role Name
=========
epel : Enable the EPEL repository

Official OS Support
-------------------

 - CentOS 7
 - RHEL 7

Role Variables
--------------
None.

Dependencies
------------
None.

Example Playbook
----------------

```
#!yaml
- name: Enable EPEL
  hosts: someserver
  remote_user: ansible
  become: yes
  roles:
     - role: epel
```

Author Information
------------------
Support <support@incognito.com>
