acs_gui
=========

Installs the ACS GUI on the server, and configures the web.xml's.


Official OS Support
------------

 - CentOS 7
 - Debian 8


Role Variables
--------------


| Variable                 | Default       | Description  |
| ------------------------ |-------------- | ------------ |
| `acs_gui_version`        | `None`        | specific version to install, e.g. `3.2.0.94`. |
| `acs_host`               | `default_ipv4.address`        | The location of the ACS.                   |
| `ums_host`               | `default_ipv4.address`   | The location of the UMS. |
| `ums_port`               | `9995`        | The CORBA port of the UMS to be set in the web.xml.        |
| `rabbitmq_port`          | `5672`        | The port of the RabbitMQ service to be set in the web.xml. |
| `ies_host`               | `default_ipv4.address`   | The location of the Incognito Ecosystem Scripting engine. |
| `acs_service_name_ums`   | `ACS {{ acs_host }}`   | The name of the ACS service as registered in the UMS |
| `ies_service_name_ums`   | `JSE {{ ies_host }}`   | The name of the IES service as registered in the UMS |
| `ui_acs_directory`       | `ui-acs`      | The installation directory. Change to 'uiacs' if installing 3.2.x |

Dependencies
------------

The following **MUST** be set in the playbook as vars:

| Variable            | Description  |
| ------------------- | ------------ |
| `repo_type`         | `Set as public`         |
| `apt_section`       | The section for debian repos, e.g. `main` or `development`.    |
| `yum_beta`          | `"yes"` or `"no"`. Whether to enable the beta yum repo or not. |
| `acs_key`           | The license key for ACS.                                       |
| `acs_major_ver`     | The major version of the repository, e.g. `3`.                 |
| `rabbitmq_user`     | The RabbitMQ user to be set in the web.xml.                    |
| `rabbitmq_password` | The password for the RabbitMQ to be set in the web.xml.        |
| `rabbitmq_host`     | The location of the RabbitMQ server, e.g. `127.0.0.1`, to be set in the web.xml. |

Example Playbook
----------------

```
#!yaml
- name: Install ACS GUI
  hosts: my_server
  remote_user: ansible
  become: yes
  vars:
    repo_type: public
    apt_section: main
    yum_beta: "no"
    acs_key: 03X-ACS003-1111-1111-1111-1111
    acs_major_ver: 3
    rabbitmq_user: ansible
    rabbitmq_password: incognito
    rabbitmq_host: 127.0.0.1
  roles:
    - role: acs_gui
```

Author Information
------------------
Support <support@incognito.com>
