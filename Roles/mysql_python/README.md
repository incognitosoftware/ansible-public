mysql_python
=========

Installs mysql_python on the server.

Official OS Support
-------------------

 - CentOS 7 + 6
 - Debian 8 + 7

Role Variables
--------------

None

Dependencies
------------

None

Example Playbook
----------------

```
#!yaml
- name: Install mysql_python
  hosts: someserver
  remote_user: ansible
  become: yes
  roles:
	- mysql_python
```


Author Information
------------------
Support <support@incognito.com>
