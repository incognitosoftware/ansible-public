Role Name
=========

ac\_stack: Installs and configures an entire AC stack on a server (IPMS, UMS, WMC, SDM, NDP, IES, DB Client, DB Server, ies-ui, sdm-ui and sdm-ies-module).

Requirements
------------
Official OS Support :

	CentOS7

Dependencies
------------
1. Installing AC default stack
The Core/Default playbook will install IPMS, UMS, NDP, WMC and Postgres DB Client.
This playbook is used when there is already existing db Server. The user will specify the db host in the ‘db_host’ parameter, as well as the username and password in ‘db_user’ and ‘db_password’ parameters respectively unless the default values satisfy the conditions.
The playbook will create automatically the services db schema and db user on the db server using the default ‘db_name’ param value.
2. Installing NDP
Installation of the NDP service component is enabled by default.
In case the user wants to exclude the NDP component, parameter ‘install_ndp’ has been set to ‘no’ in the playbook explicitly.
3. Setting up db client
The DB client installation is enabled by default. The type of the  connector is determined by the type of the database set via ‘db_type’ parameter.  
In case the db client is preconfigured and the user wants to exclude it from the installation process, ‘install_db_client’ parameter has to explicitly be set to ‘no’ in the playbook.
4. Setting/Defining DB Service
The installation of a DB service is not enabled by default, in which case it is assumed that the service is pre configured. The location of the db host is specified via ‘db_host’ parameter.
In case the user wants to include installation and setup of a DB service. the ‘install_db_service’ needs to be set to ‘yes’. The Role supports db server installation on the target services host only .
5. Setting MySql db user
By default the user will be created at the target db host. Parameters ‘db_user’ and ‘db_password’ are used to set the username and password.
In case the user exists and is not necessary to create it, it can be excluded from the stack installation by setting ‘install_mysql_user’ to ‘no’ in the playbook explicitly.
6. Adding SDM/IES components to the Core installation can be accomplished by setting ‘install_sdm’ to ‘yes’. The following is an example playbook. Please note that specific versions are needed for ‘ies_gui_version’ and ‘ies_version’ parameters, since the latest is not compatible.
7. Upgrade Services
Ac_stack supports services upgrade. After using ansible to upgrade the services, odbc.ini OR config files for each service might needs to be manually updated if neccessary.

Role Variables
--------------
NDP Related Vars
--------------
| Variable                 | Default       | Description  |
| ------------------------ |-------------- | ------------ |
| `install_ndp`       | `yes`   | This parameter is used to optionally install the NDP related.  |
| `ndp_key`       	  | None     | The license key for NDP.  |
| `ndp_multicast_region_id` | None | `1-255`. Value to be set for `MULTICAST_NOTIFICATION_REGION_ID` in the NDP configuration file.  |
--------------
DB Related Vars
--------------
| Variable                 | Default       | Description  |
| ------------------------ |-------------- | ------------ |
| `db_type`       | `postgres` Notes:[postgres] Due to an Ansible issue for some versions (2.1.0.0, 2.1.1.0, perhaps others), the postgresql setup will not work unless you have this set in the ~/ansible.cfg: allow_world_readable_tmpfiles = True | `mysql` or `postgres`. The database type for AC stack. |
| `db_host`       | `localhost`       | The location of the database host. |
| `db_user`       | `incognito`       | The database user for the AC database. |
| `db_password`   | `incognito`       | The password for the database user. |
| `create_ac_db`  | `yes`             | 'yes' or 'no'- Used to specify if a new user is needed for the db. |
| `db_name`       | `ac_stack`       | The database name for ac stack services. |
| `install_db_service`       | `yes`       | The user can choose to install a db service for the db type specified in the ‘db_type’ param. If set to ‘no’, a db server will not be installed. This is useful in cases where the server is already installed. The default value is set to ‘no’. |
| `install_db_client`       | `yes`       | The user can choose to install a db client for the db type specified in the ‘db_type’ param. If set to ‘no’, a db client will not be installed. This is useful in cases where the client is already installed. The default value is set to ‘yes’. |
| `set_db_odbc_file`       | `yes`       | 'yes' or 'no' - Used to specify if odbc file needs to be updated. Useful cases are during fresh installation or when during upgrade the db specifics are changed.|
--------------
Additional DB Related Parameters:
--------------
| Variable                 | Default       | Description  |
| ------------------------ |-------------- | ------------ |
| `pg_admin_name`       | `postgres`       | The name of the admin user for the PostgreSQL server. |
| `postgresql_version` | `9.5` | The version of PostgreSQL to install. See the `postgresql` role for OS availability.                           |
| `install_mysql_user` | `yes`   | 'yes' or 'no' - Used to specify if a new mysql user is needed.         |
| `mysql_user_priv`        | `"*.*:ALL"`   | Set the privilege for mysql user. |
| `mysql_root_password`    | `incognito`   | The password for the root MySQL user.  |
| `mysql_version`        | `5.7`   | `5.7`. The version of MySQL Server to install.     |
| `mysql_host_username`  | `["localhost","%"]`   | The 'host' part of the MySQL username which can be a JSON formatted string to add multiple IP/hostnames. |
--------------
SDM Related Vars
--------------
| Variable                 | Default       | Description  |
| ------------------------ |-------------- | ------------ |
| `install_sdm`  | `no`   | This parameter is used to optionally install the SDM related components. If this parameter value is “no”, then “- role: incognito-ecosystem-scripting-sdm-module” should be removed in the playbook.|
| `sdm_key`      | None   | The license key for SDM.     |
| `ies_gui_version`   | None   | Specific version of IES GUI to install, e.g. 1.2.2.1. |
| `ies_version`  | None | Specific version of IES to install, e.g. 1.2.3.1. |
| `ies_multicast_region_id` | None | 1-255. Value to be set for MULTICAST_NOTIFICATION_REGION_ID in the IES configuration file. |
--------------
ISMP Related Vars
--------------
| Variable                 | Default       | Description  |
| ------------------------ |-------------- | ------------ |
| `install_ismp`   | `no`   | `yes` or `no`- Used to optionally install the ISMP related components. |
| `ismp_key`       | None   | The license key for ISMP.  |
| `ac_ismp_multicast_address` | `229.255.0.1:16000`   | The IMSP Multicast address. |
--------------
Rabbitmq Related Vars
--------------
| Variable                 | Default       | Description  |
| ------------------------ |-------------- | ------------ |
| `rabbitmq_username`| `guest`    | The RabbitMQ user to be created and used. |
| `rabbitmq_password`    | `guest`   | The password for the RabbitMQ user that is created. |
| `rabbitmq_repo`  | None | `epel` works for AC. Need to set it to epel. |
| `rabbitmq_host`| `127.0.0.1` | The RabbitMQ host to be used. |
--------------
Additional Role Vars
--------------
| Variable                 | Default       | Description  |
| ------------------------ |-------------- | ------------ |
| `ums_major_ver`  | `5` | The major version of the UMS service - used for repository configuration. |
| `ipms_major_ver` | `5` | The major version of the IPMS service - used for repository configuration. |
| `ancwmc_major_ver` | 5 | The major version of the WMC service - used for repository configuration. |
| `ndp_major_ver`| `1` | The major version of the NDP service - used for repository configuration. |
| `sdm_major_ver`| `1` | The major version of the SDM service - used for repository configuration. |
| `ies_major_ver`| `1` | The major version of the IES service - used for repository configuration. |
| `ismp_major_ver`  | `5` | The major version of the ISMP service - used for repository configuration. |
| `ac_stack_ums_port`| `9995` | The port for UMS to listen on. |

Dependencies
------------

The following **MUST** be set in the playbook as vars:

| Variable         | Description  |
| ---------------- | ------------ |
| `repo_type`      | `local` or `public`. The repository type to pull from. |
| `yum_beta`       | "yes"` or `"no"`- Used to spcify Whether to enable the beta yum repo or not. |
| `apt_section`    | `Type of repo. Usually it's "main" for ga and "development" for latest` |
| `ipms_key`       | `License key for IPMS` |
| `ums_key`        | `License key for UMS`  |


Example Playbook
----------------

```
#!yaml
- name: Setup of AC Stack from Public Example
  hosts: changeme
  remote_user: ansible
  become: yes
  vars:
    repo_type: public
    apt_section: "main"
    yum_beta: "no"
    ipms_key: changeme
    ums_key: changeme

    # [ndp component,default is to install ndp]
    install_ndp: yes
    ndp_key: changeme

    # [database settings]
    # The settings here will support updating the Data Source Name(DSN) in odbc.ini file
    # Specify database source and update database type specific settings below
    db_type: changeme
    db_host: changeme
    db_user: changeme
    db_password: changeme
    create_ac_db: yes
    db_name: ac
    set_db_odbc_file: yes

    # [ mysql DB settings, default is to create mysql db, user and install mysql service and client]
    install_db_service: yes
    install_db_client: yes
    install_mysql_user: yes
    mysql_user_priv: "*.*:ALL"
    # [Database supports MySQL®5.7+.]
    mysql_version: 5.7
    mysql_root_password: incognito

    # [ postgres DB settings, default is to install postgresql, client and create db. Database supports PostgreSQL®9.x.]
    install_db_client: yes
    postgresql_version:  9.5            

    # [Optional sdm components ]
    # Change install_sdm to “yes” and add sdm_key to install sdm components for DHCP scope deployment feature.
    install_sdm: no
    sdm_key: changeme
    ies_gui_version: 1.2.2.1
    ies_version: 1.2.4.2
    rabbitmq_repo: epel
    rabbitmq_username: guest
    rabbitmq_password: guest

  roles:
     - role: ac_stack
     # This role is optional, only keep it when installing ies and sdm.
     # This line needs to be removed if install_sdm: yes
     # - role: incognito-ecosystem-scripting-sdm-module
```


Author Information
------------------

Jennifer Yang <jennifer.yang@incognito.com>
