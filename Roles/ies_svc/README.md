ies\_svc
=========

Installs IES on the server, configures, and starts it.

**Note:** this role does not configure the database for IES. To do that, use the `ies_db` role.


Official OS Support 
-------------------

 - CentOS 7
 - Debian 8


Role Variables
--------------

| Variable                 | Default       | Description  |
| ------------------------ |-------------- | ------------ |
| `ies_version`            | None          | Specific version of IES to install, e.g. `1.2.0.10`. |
| `ies_db_type`            | `mysql`       | `mysql` or `postgres`. The database type for IES. |
| `ies_db_host`            | `localhost`   | The location of the database IES is using. |
| `ies_db_port`            | MySQL: `3306`, PostgreSQL: `5432` | The port used by the IES database server. |
| `ies_db_name`            | `ies`         | The database name for IES. |
| `ies_multicast_address`  | `229.255.0.1:16000` | `ip:port`. Value to be set for `MULTICAST_ADDRESS` in the IES configuration file. |
| `ies_ums_port`           | `9995`        | The port UMS is listening on.                           |
| `ies_ums_host`           | `default_ipv4.address`   | The location of the UMS. |
| `rabbitmq_port`          | `5672`        | The port of the RabbitMQ service. |

Modules
--------------
This is a list of IES modules and their corresponding Roles.

| Role                | Description  |
| ------------------- |-------------- | ------------ |
| `ies_dhcp_module`   | Installs the DHCP module. |
| `ies_cls_module`    | Installs the CLS module.  |


Dependencies
------------

| Variable                 | Description  |
| ------------------------ |------------- |
| `repo_type`              | `Set as public`              |
| `apt_section`            | The section for debian repos, e.g. `main` or `development`.          |
| `yum_beta`               | `"yes"` or `"no"`. Whether to enable the beta yum repo or not.        |
| `rabbitmq_user`          | The value for `MESSAGE_BROKER_USERNAME` to be set in the IES config. |
| `rabbitmq_password`      | The value for `MESSAGE_BROKER_PASSWORD` to be set in the IES config. |
| `rabbitmq_host`          | The address of the RabbitMQ server, e.g. `127.0.0.1`. Used in conjunction with `rabbitmq_port` to set MESSAGE_BROKER_ADDRESS in the IES config. |
| `ies_multicast_region_id`| `1-255`. Value to be set for `MULTICAST_NOTIFICATION_REGION_ID` in the IES configuration file. |
| `ies_major_ver`          | Specific version of IES to install, e.g. `2`. |
     			
**Required for MySQL:**

| Variable                 | Description  |
| ------------------------ |------------- |
| `mysql_user`             | The database user for IES.     |
| `mysql_user_password`    | The user's password. |

**Required for PostgreSQL:**

| Variable                | Description  |
| ----------------------- | ------------ |
| `pg_management_user`    | The name of the management user to be created for the database. |
| `pg_management_password`| The password for the management user.                           |


Example Playbook
----------------

```
#!yaml
- name: Install IES
  hosts: my_server
  remote_user: ansible
  become: yes
  vars:
    repo_type: public
    apt_section: "main"
    yum_beta: "no"
    rabbitmq_user: ansible
    rabbitmq_password: incognito
    rabbitmq_host: 127.0.0.1
    mysql_user: ansible
    mysql_user_password: incognito
    ies_major_ver: 2
    ies_ums_host: 127.0.0.1
    ies_multicast_region_id: 1
  roles:
    - ies_svc
```

Author Information
------------------
Support <support@incognito.com>
