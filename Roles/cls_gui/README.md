Role Name
=========

cls\_gui : Installs the Central Lease Service GUI on the server.

Official OS Support
------------

The OS Support expected to work for this role

 - CentOS 7
 - Debian 8

Role Variables
--------------

**Note** : The latest version will be installed by default. Only when you do NOT want the latest do you need to define a specific version.

| Variable             | Default       | Description  |
| -------------------- |-------------- | ------------ |
| `cls_gui_version`    | `None`        | Specific version to install, e.g `1.1.0.38`. |


Dependencies
------------

The following **MUST** be set in the playbook as vars:

| Variable           | Description  |
| ------------------ | ------------ |
|`repo_type`      | `Set as public` |
|`apt_section`  | `Type of repo. Typically this is set as "main".`|
|`yum_beta`   | `Should yum beta repository be enabled. Options are "no" or "yes"`|
|`cls_key`           | The license key for CLS.                                       |
|`cls_major_version` | The major version of the repository, e.g. `2`.                 |


Example Playbook
----------------

```
#!yaml
- name: Install CLS GUI
  hosts: my_server
  remote_user: ansible
  become: yes
  vars:
    repo_type: public
    apt_section: "main"
    yum_beta: "no"
    cls_major_version: 2
    cls_key: 02X-CLS001-1111-1111-1111-1111
  roles:
    - role: cls_gui
```


Author Information
------------------
Support <support@incognito.com>
