Role Name
=========

acs_stack : Installs and configures an entire ACS stack on a server (ACS, CWMP, UMS, IES, DB Client)


Requirements
------------
Official OS Support :

	CentOS 7
	Debian 8


Role Variables
--------------

| Variable                 			   | Default        | Description  |
| -------------------------------- |------------------------------- | ------------ |
| `ums_db_name` 	           | `ums`          		    | The name of the UMS database. |
| `ies_db_name`			   | `ies`         		    | The name of the IES database. |
| `ums_port`               	   | `9995`                         | The port for UMS to listen on.|
| `acs_db_user`            	   | `incognito`	   	    |  Username for ACS database. |
| `acs_db_password`        	   | `incognito`		    |  Password for ACS database. |

Dependencies
------------

The following **MUST** be set in the playbook as vars:

| Variable         		  | Description  |
| ------------------------------- | ------------ |
| `repo_type`              	  | `Set as public` |
| `apt_section`	           	  | Type of repository for Debian systems. Options are `"main"` or `"development"`.|
| `yum_beta`	                  | Type of repository for CentOS/EL systems. Options are `"no"` or `"yes"`.|
| `acs_key`        		  | License key for ACS. |
| `cwmp_key`   		          | License key for CWMP. |
| `ums_key`        		  | License key for UMS.  |
| `acs_major_ver` 	          | The major version of the ACS service. |
| `cwmp_major_ver` 		  | The major version of the CWMP service. |
| `ums_major_ver` 	          | The major version of the UMS service. |
| `ies_major_ver` 		  | The major version of the IES service. |
| `rabbitmq_host`                 | The address of the RabbitMQ service. |
| `rabbitmq_user`      	          | The name of the RabbitMQ user to create.|
| `rabbitmq_password`  	          | The password for the RabbitMQ user that is created. |
| `multicast_region_id`		  | `1-255`. Value to be set for multicast region ID. |
| `db_type`                	  | `mysql` or `postgres`. The database type for ACS.       |
| `db_port`                	  | The port used by the ACS database server. For example, `3306` for MySQL or `5432` for PostgreSQL. |

**Required for MySQL:**

| Variable                 | Description  |
| ------------------------ |------------- |
| `mysql_user`             | Username for MySQL user.|
| `mysql_user_password`    | Password for MySQL user. |
| `mysql_root_password`    | Password for root MySQL user|
| `mysql_version`          | MySQL version to install, ie. 5.5 |


Example Playbook using MySQL
-------------------------------
```
- name: Install ACS
  hosts: someserver
  remote_user: ansible
  become: yes
  vars:
    repo_type: public
    apt_section: "main"
    yum_beta: "no"
    acs_key: 03X-ACS001-1111-1111-1111-1111
    cwmp_key: 03X-CWM001-1111-1111-1111-1111
    ums_key: 05X-RCM100-1111-1111-1111-1111
    acs_major_ver: 3
    cwmp_major_ver: 3
    ums_major_ver: 5
    ies_major_ver: 1
    rabbitmq_host: localhost
    rabbitmq_user: incognito
    rabbitmq_password: incognito
    multicast_region_id: 135
    db_type: mysql
    db_port: 3306
    mysql_user: incognito
    mysql_user_password: incognito
    mysql_root_password: incognito
    mysql_version: 5.5
  roles:
    - role: acs_stack
```

Author Information
------------------
Support <support@incognito.com>
