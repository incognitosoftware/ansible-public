Role Name
=========

incognito\_repository\_ndp :  Configure Incognito repository to allow NDP packages to be installed

Official OS Support
-------------------

- CentOS : 6 + 7

Role Variables
--------------

None

Dependencies
------------

The following **MUST** be set in the playbook as vars :

| Variable         | Description  |
| ---------------- | ------------ |
|`repo_type`      | `Options are local or public` |
|`apt_section`	  | `Type of repo`|
|`yum_beta`	   | `Should yum beta repository be enabled. Options are "no" or "yes"`|
|`ndp_key`	   | `License key for NDP`|
|`ndp_major_ver` | `Major version of repository e.g. 1` |


Example Playbook
----------------

```
#!yaml
- name: Setup of XYZ
  hosts: someserver
  remote_user: ansible
  become: yes
  vars:
    repo_type: public
    apt_section: "main"
    yum_beta: "no"
    ndp_key: 05X-NDP100-1111-1111-1111-1111
    ndp_major_ver: 5
  roles:
	- role: incognito_repository_ndp
```


Author Information
------------------

Jennifer Yang <jennifer.yang@incognito.com>
