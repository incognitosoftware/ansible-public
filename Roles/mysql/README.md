mysql
=========

Installs mysql-server on the server, and sets the root MySQL user password.
Upgrading between MySQL 5.5, 5.6 and 5.7 is supported. Downgrading is not.
Simply specify the target version in `mysql_version` and set `upgrade_mysql` to `true` in order to upgrade.

**Note:** The role creates the file `/root/.my.cnf`, which is used by the role in future runs on the server to determine the mysql root user's credentials.


Official OS Support
------------

 - CentOS 6, 7
 - Debian 7, 8


Role Variables
--------------

| Variable              | Default   | Description  |
| --------------------- | --------- | ------------ |
| `mysql_bind_address`  | `0.0.0.0` | address for mysql to bind to. |
| `mysql_thread_stack`  | `256K`    | The stack size for each MySQL Server thread. |
| `upgrade_mysql`       | `false`   | Set to true if you are upgrading MySQL. |

Dependencies
------------

| Variable              | Description  |
| --------------------- | ------------ |
| `mysql_root_password` | The password to be set for the MySQL root user.  |
| `mysql_version`       | `5.5`, `5.6`, or `5.7`. The version of MySQL Server to install. |


Example Playbook
----------------

Example to install MySQL Server

```
#!yaml
- name: Install mysql
  hosts: my_server
  remote_user: ansible
  become: yes
  vars:
    mysql_root_password: def5678
    mysql_version: 5.5
  roles:
    - mysql
```

Author Information
------------------
Support <support@incognito.com>
