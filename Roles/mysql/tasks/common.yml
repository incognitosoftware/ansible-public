---

# Initially remove the bind-address line due to bug in ini_file
# https://github.com/ansible/ansible-modules-core/issues/106
- name: Remove bind-address line in my.cnf with spaces
  lineinfile:
    regexp: "bind-address"
    dest: "{{ mysql_config }}"
    state: absent

- name: Update bind address in my.cnf
  ini_file:
    dest: "{{ mysql_config }}"
    section: mysqld
    option: bind-address
    value: "{{ mysql_bind_address }}"
  register: update_bind_address

- name: Restart mysql if configuration file changed
  service:
    name: "{{ mysql_service }}"
    state: restarted
  when: update_bind_address.changed

- name: Set MySQL to start on boot
  service:
    name: "{{ mysql_service }}"
    enabled: yes

- name: Run mysql_upgrade if upgrading
  command: mysql_upgrade chdir=/var/lib/mysql
  when:
    - mysql.changed
    - upgrade_mysql

- name: Set MySQL root password for root account on all potential hosts
  mysql_user:
    name: root
    host: "{{ mysql_db_host }}"
    password: "{{ mysql_root_password }}"
  with_items:
    - "{{ ansible_default_ipv4.address }}"
    - "{{ ansible_hostname }}"
    - 127.0.0.1
    - ::1
    - localhost
  loop_control:
    loop_var: mysql_db_host

- name: Copy .my.cnf file to server to save updated credentials
  template:
    src: my.cnf.j2
    dest: /root/.my.cnf
    owner: root
    mode: 0600

- name: Remove the MySQL test database
  mysql_db:
    db: test
    state: absent
