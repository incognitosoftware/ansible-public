---

- name: UMS MySQL Database
  mysql_db:
    login_host: 127.0.0.1
    login_user: root
    login_password: "{{ mysql_root_password }}"
    collation: latin1_general_ci
    encoding: latin1
    name: "{{ ums_db_name }}"
    state: present
  when: ums_db_type == "mysql"

- name: UMS MySQL user
  mysql_user:
    login_user: root
    login_password: "{{ mysql_root_password }}"
    login_host: 127.0.0.1
    host: '%'
    name: "{{ mysql_user }}"
    password: "{{ mysql_user_password }}"
    priv: "{{ ums_db_name }}.*:ALL,GRANT"
    state: present
  when: ums_db_type == "mysql"

- name: Install python-psycopg2
  package:
    name: python-psycopg2
    state: latest
  when: ums_db_type == "postgres"

- name: Install acl so pg_admin_user can perform actions
  package:
    name: acl
    state: latest
  when: ums_db_type == "postgres"

- name: Create PostgreSQL database
  become: true
  become_user: postgres
  postgresql_db:
    name: "{{ ums_db_name }}"
    login_user: "{{ pg_admin_user }}"
    login_host: 127.0.0.1
    lc_collate: "{{ pg_locale }}"
    lc_ctype: "{{ pg_locale }}"
    encoding: "{{ pg_encoding }}"
    template: "{{ pg_template }}"
  when: ums_db_type == "postgres"

- name: Create Management user for the PostgreSQL database
  become: true
  become_user: "{{ pg_admin_user }}"
  postgresql_user:
    name: "{{ pg_management_user }}"
    password: "{{ pg_management_password }}"
    login_user: "{{ pg_admin_user }}"
    login_host: 127.0.0.1
    db: "{{ ums_db_name }}"
    role_attr_flags: SUPERUSER,CREATEDB,CREATEROLE
    priv: ALL
  when: ums_db_type == "postgres"

- name: Set the procedural language for the database
  become: true
  become_user: "{{ pg_admin_user }}"
  postgresql_lang:
    login_user: "{{ pg_admin_user }}"
    login_host: 127.0.0.1
    db: "{{ ums_db_name }}"
    lang: plpgsql
  when: ums_db_type == "postgres"
