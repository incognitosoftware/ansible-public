ums_db
=========

Installs either PostgreSQL or MySQL on the server and creates a database for UMS.

Official OS Support
-------------------

 - CentOS 7
 - Debian 8


Role Variables
--------------

| Variable                 | Default       | Description  |
| ------------------------ |-------------- | ------------ |
| `ums_db_type`            | `mysql`       | `mysql` or `postgres`. The database type for UMS. |
| `ums_db_name`            | `ums`         | The database name for UMS. |


Dependencies
------------

**Required for MySQL:**

| Variable                 | Description  |
| ------------------------ |------------- |
| `mysql_user`             | The database user for UMS.     |
| `mysql_user_password`    | The user's password. |
| `mysql_root_password`    | The password for the root MySQL user. |
| `mysql_version`          | `5.5`, `5.6`, or `5.7`. The version of MySQL Server to install. |


**Required for PostgreSQL:**

| Variable                | Description  |
| ----------------------- | ------------ |
| `pg_management_user`    | The name of the management user to be created for the database. |
| `pg_management_password`| The password for the management user.                           |
| `postgresql_version`    | The version of PostgreSQL to install. See the `postgresql` role for OS availability. |


Example Playbook
----------------

```
#!yaml
- name: Set up UMS database
  hosts: my_server
  remote_user: ansible
  become: yes
  vars:
    ums_db_name: ums_ansible
    ums_db_type: postgres
    pg_management_user: ansible
    pg_management_password: incognito
    postgresql_version: 9.1
  roles:
    - ums_db
```

Author Information
------------------
Support <support@incognito.com>
