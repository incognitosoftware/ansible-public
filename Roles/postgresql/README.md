postgresql
=========

Installs and configures PostgreSQL server.

Official OS Support
------------

 - CentOS 7
 - Debian 8


Role Variables
--------------

None

Dependencies
------------

| Variable              | Description  |
| --------------------- | ------------ |
| `postgresql_version`  | The version of PostgreSQL to install (see below for availability). |

OS Availability
---------------

The following available versions may change in the future.

| OS       | Available versions |
| -------- | ------------------ |
| Debian 8 | `9.4, 9.5`         |
| CentOS 7 | `9.2, 9.3, 9.4, 9.5, 9.6`                |

Example Playbook
----------------

```
#!yaml
- name: Install PostgreSQL Server 
  hosts: someserver
  remote_user: ansible
  become: yes
  vars:
    postgresql_version: 9.4
  roles:
    - postgresql
```

Author Information
------------------
Support <support@incognito.com>
