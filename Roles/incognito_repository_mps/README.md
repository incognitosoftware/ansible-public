Role Name
=========

incognito\_repository\_mps :  Configure Incognito repository to allow MPS packages to be installed

Official OS Support
-------------------

- CentOS : 7
- Debian : 8

Role Variables
--------------

None

Dependencies
------------

The following **MUST** be set in the playbook as vars :

| Variable         | Description  |
| ---------------- | ------------ |
|`repo_type`      | `Set as public` |
|`apt_section`  | `Type of repo. Typically this is set as "main".`|
|`yum_beta`   | `Should yum beta repository be enabled. Options are "no" or "yes"`|
|`mps_key`   | `License key for MPS`|
|`bcc_major_ver` | `Major version of repository e.g. 7` |


Example Playbook
----------------

```
#!yaml
- name: Setup of XYZ
  hosts: someserver
  remote_user: ansible
  become: yes
  vars:
    repo_type: public
    apt_section: "main"
    yum_beta: "no"
    mps_key: 07X-DPM001-1111-1111-1111-1111
    bcc_major_ver: 7
  roles:
     - role: incognito_repository_mps
```


Author Information
------------------

Support <support@incognito.com>
