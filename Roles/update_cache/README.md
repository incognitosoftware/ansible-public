Role Name
=========

update\_cache :  Update the local package manager cache for Debian based systems.

**Note** : yum operates in a different manner and does not require the cache to be updated in this manner.

The last time `apt-get update` was performed can be found with

```
$ stat -c %y /var/cache/apt/
```

Official OS Support
-------------------

    Debian 8

Role Variables
--------------

| Variable                 | Default       | Description  |
| ------------------------ |-------------- | ------------ |
|`apt_cache_valid_time`    | `1800`         | `Time in seconds ( e.g. 1800 seconds/30 minutes). If the last run of apt-get update is <= this value in seconds, it will be skipped`|

Dependencies
------------

None

Example Playbook
----------------

```
#!yaml
- name: Setup of XYZ
  hosts: someserver
  remote_user: ansible
  become: yes
  vars:
    apt_cache_valid_time: 6400
  roles:
    - role: update_cache
```


Author Information
------------------
Support <support@incognito.com>
