Role Name
=========

cfm\_svc :  Install CFM service, set administrator password and register service

Official OS Support
-------------------

 - CentOS : 7
 - Debian : 8

Role Variables
--------------

**Note** : The latest version will be installed by default. Only when you do NOT want the latest do you need to define a specific version.

| Variable                 | Default       | Description  |
| ------------------------ |-------------- | ------------ |
| `cfm_svc_version`      | `None`| `Specific version to install e.g. 6.6.1.11 or if there are more than one revision of the same version e.g. 6.5.3.1-2)` |
| `cfm_corba_port`      | `9972`| `Service CORBA port to run any CLI commands` |



Dependencies
------------

The following **MUST** be set in the playbook as vars :

| Variable         | Description  |
| ---------------- | ------------ |
|`repo_type`      | `Set as public` |
|`apt_section`  | `Type of repo. Typically this is set as "main".`|
|`yum_beta`        | `Should yum beta repository be enabled. Options are "no" or "yes"`|
|`cfm_key`   | `License key for CFM`|
|`bcc_major_ver` | `Major version of repository e.g. 7` |
|`bcc_admin_password` | `Set the administrator account password. Note: Password must be between 5 and 31 characters and CANNOT be the default of "incognito"` |

Example Playbook
----------------

```
#!yaml
- name: Setup of XYZ
  hosts: someserver
  remote_user: ansible
  become: yes
  vars:
    repo_type: public
    apt_section: "main"
    yum_beta: "no"
    cfm_key: 07X-FGN100-1111-1111-1111-1111
    bcc_major_ver: 7
    bcc_admin_password : ansible
  roles:
     - role: cfm_svc
```


Author Information
------------------

Support <support@incognito.com>
