Role Name
=========

tomcat : Installs Tomcat on the server, configures it, and starts it.

Official OS Support
------------

 - CentOS 7
 - Debian 8

Role Variables
--------------

| Variable             | Default       | Description  |
| -------------------- |-------------- | ------------ |
| `initial_heap_size`  | `128M`        | The initial java heap size allocated to tomcat. |
| `max_heap_size`      | `256M`        | The maximum heap size that can be allocated to tomcat. |


Dependencies
------------

None

Example Playbook
----------------

```
#!yaml
- name: Install Tomcat
  hosts: my_server
  remote_user: ansible
  become: yes
  roles:
    - role: tomcat
```


Author Information
------------------
Support <support@incognito.com>
