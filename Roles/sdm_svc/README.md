sdm\_svc
=========

Installs SDM on the server, configures, and starts it.

Official OS Support
------------

 - CentOS 7


Role Variables
--------------

| Variable                 | Default       | Description  |
| ------------------------ |-------------- | ------------ |
| `sdm_version`            | None          | Specific version of SDM to install, e.g. `1.0.4.8`. |
| `sdm_db_type`            | `mysql`       | `mysql` or `postgres`. The database type for SDM.       |
| `sdm_db_name`            | `sdm`         | The database name for SDM. |
| `sdm_db_host`            | `localhost`   | The location of the database.                           |
| `sdm_multicast_address`  | None          | `ip:port`. Value to be set for `MULTICAST_ADDRESS` in the SDM configuration file. |
| `sdm_multicast_region_id`| None          | `1-255`. Value to be set for `MULTICAST_NOTIFICATION_REGION_ID` in the SDM configuration file. |
| `sdm_repository_base_url`| `http://assets.incognito.com`    | Value to be set for `REPOSITORY_BASE_URL` in the SDM configuration file. |
| `ums_port`           | `9995`        | The port for UMS to listen on.                          |
| `sdm_ums_host`           | `default_ipv4.address`   | The location of the UMS. |
| `rabbitmq_port`          | `5672`        | The port of the RabbitMQ service. |  
| `DATA_PURGE_START_TIME`          | `1`        | Specifies the hour at which the data purge will begin. Valid range [0, 23]. The default is 1. |
| `LOGLEVEL`          | `1`        | The level of SDM logs. |


Dependencies
------------

The following **MUST** be set in the playbook as vars :

| Variable         | Description  |
| ---------------- | ------------ |
| `repo_type`      | `Options are local or public` |
| `apt_section`	  | `Type of repo`|
| `yum_beta`	   | `Should yum beta repository be enabled. Options are "no" or "yes"`|
| `sdm_key`	   | `License key for SDM`|
| `sdm_major_ver` | `Major version of repository e.g. 1` |
| `rabbitmq_user`          | The value for `MESSAGE_BROKER_USERNAME` to be set in the SDM config. |
| `rabbitmq_password`      | The value for `MESSAGE_BROKER_PASSWORD` to be set in the SDM config. |
| `rabbitmq_host`          | The address of the RabbitMQ server, e.g. `127.0.0.1`. Used in conjunction with `rabbitmq_port` to set MESSAGE_BROKER_ADDRESS in the SDM config. |
| `sdm_db_user`             | The database user for SDM.     |
| `sdm_db_password`    | The user's password. |


Example Playbook
----------------

```
#!yaml
- name: Install SDM
  hosts: my_server
  remote_user: ansible
  become: yes
  vars:
    repo_type: "public"
    apt_section: "main"
    yum_beta: "no"
    sdm_key: 01X-SDM001-1111-1111-1111-1111
    sdm_major_ver: 1
    sdm_db_type: postgres
    sdm_db_host: 1.2.3.4
    sdm_db_user: incognito
    sdm_db_password: incognito
    rabbitmq_host: localhost
    rabbitmq_user: secret
    rabbitmq_password: secret
  roles:
    - sdm_svc
```


Author Information
------------------
Jennifer Yang <jennifer.yang@incognito.com>
