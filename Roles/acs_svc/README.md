Role name
=========

acs_svc: Installs ACS on the target server.

Official OS Support
------------

 - CentOS 7
 - Debian 8


Role Variables
--------------

| Variable                 | Default                           | Description  |
| ------------------------ |---------------------------------- | ------------ |
| `acs_version`            | None                              | Specific version of ACS to install, e.g. `3.3.0.11`. |
| `rabbitmq_port`          | `5672`                            | The port of the RabbitMQ service. |  
| `ums_host`               | `localhost`                       | The location of the UMS. |
| `ums_port`               | `9995`                            | The port for UMS to listen on. |
| `acs_db_name`            | `acs`                             | The database name for ACS. |
| `db_type`                | `mysql`                           | `mysql` or `postgres`. The database type for ACS.       |
| `db_host`                | `localhost`                       | The location of the database.                           |
| `repository_base_url`    | `http://assets.incognito.com`     | The repository base url for ACS. |
| `acs_db_user`            | `incognito`	   	               |  Username for ACS database. |
| `acs_db_password`        | `incognito`		               |  Password for ACS database. |


Dependencies
------------

The following **MUST** be set in the playbook as vars :

| Variable                 | Description  |
| ------------------------ | ------------ |
| `repo_type`              | `Set as public` |
| `apt_section`	           | Type of repository for Debian systems. Options are `"main"` or `"development"`|
| `yum_beta`	             | Type of repository for CentOS/EL systems. Options are `"no"` or `"yes"`|
| `acs_key`	               | License key for ACS. |
| `acs_major_ver`          | Major version for ACS |
| `rabbitmq_host`          | The address of the RabbitMQ service. |  
| `rabbitmq_user`          | The username of the RabbitMQ service. |  
| `rabbitmq_password`      | The password of the RabbitMQ service. |  
| `db_port`                | The port used by the ACS database server. For example, `3306` for MySQL or `5432` for PostgreSQL. |
| `multicast_region_id`    | `1-255`. Value for the multicast region ID. |

**Required for MySQL:**

| Variable                 | Description  |
| ------------------------ |------------- |
| `mysql_user`             | Username for MySQL user.|
| `mysql_user_password`    | Password for MySQL user. |
| `mysql_root_password`    | Password for root MySQL user|
| `mysql_version`					 | MySQL version to install, ie. 5.5 |

**Required for PostgreSQL:**

| Variable                | Description  |
| ----------------------- | ------------ |
| `pg_management_user`    | Username for PostgreSQL management user.|
| `pg_management_password`| Password for PostgreSQL management user.|
| `postgresql_version`    | PostgreSQL version to install, ie. 9.6. |


Example Playbook
----------------

```
- name: Install ACS
  hosts: someserver
  remote_user: ansible
  become: yes
  vars:
    repo_type: public
    apt_section: "main"
    yum_beta: "no"
    acs_key: 03X-ACS001-1111-1111-1111-1111
    acs_major_ver: 3
    rabbitmq_host: localhost
    rabbitmq_user: incognito
    rabbitmq_password: incognito
    db_port: 3306
    multicast_region_id: 25
    db_type: mysql
    db_port: 3306
    mysql_user: ansible
    mysql_user_password: secret
    mysql_root_password: secret
    mysql_version: 5.5
  roles:
    - role: acs_svc

```


Author Information
------------------
Support <support@incognito.com>
