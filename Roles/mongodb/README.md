Role Name
=========

mongodb : Install and start MongoDB.

Official OS Support
------------

The OS Support expected to work for this role

 - CentOS 7
 - Debian 8

Role Variables
--------------

| Variable             | Default       | Description  |
| -------------------- |-------------- | ------------ |
| `mongo_version`      | `3.2`         | The version of mongodb to install. |


Dependencies
------------

None

Example Playbook
----------------

```
#!yaml
- name: Install MongoDB
  hosts: my_server
  remote_user: ansible
  become: yes
  roles:
    - role: mongodb
```

Author Information
------------------
Support <support@incognito.com>

