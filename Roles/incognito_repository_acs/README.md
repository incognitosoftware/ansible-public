Role Name
=========

incognito\_repository\_acs :  Configure Incognito repository to allow ACS packages to be installed

Official OS Support
-------------------

 - CentOS 7
 - Debian 8

Role Variables
--------------

None

Dependencies
------------

The following **MUST** be set in the playbook as vars :

| Variable         | Description  |
| ---------------- | ------------ |
|`repo_type`      | `Set as public` |
|`apt_section`	  | `Type of repo. Usually it's "main" for ga and "development" for latest`|
|`yum_beta`	   | `Should yum beta repository be enabled. Options are "yes" or "no"` |
|`acs_key`	   | `License key for ACS` |
|`acs_major_ver` | `Major version of repository e.g. 3` |


Example Playbook
----------------

```
#!yaml
  - name: Setup of XYZ
    hosts: someserver
    remote_user: ansible
    become: yes
    vars:
      repo_type: public
      apt_section: "main"
      yum_beta: "no"
      acs_key: 03X-ACS001-1111-1111-1111-1111
      acs_major_ver: 3
    roles:
      - role: incognito_repository_acs
```


Author Information
------------------
Support <support@incognito.com>
