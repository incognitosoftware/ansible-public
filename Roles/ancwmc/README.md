ancwmc
=========

ancwmc : Installs ANCWMC on the server.

Official OS Support
------------

 - CentOS 7

Role Variables
--------------

| Variable             | Default        | Description  |
| -------------------- |--------------- | ------------ |
| `ancwmc_host`           | `eth0 IP`    | The location of the ANCWMC. |
| `ancwmc_version`    | None           | Specific version of the ANCWMC to install, e.g. `5.10.3.7`. |
| `ancwmctomcat7_version`    | None           | Specific version of the ancwmc-tomcat7 to install, e.g. `1.0.0.1`. |

Dependencies
------------

The following **MUST** be set in the playbook as vars :

| Variable           | Description  |
| ------------------ | ------------ |
|`repo_type`         | `local` or `public`. The repository type to pull from.         |
|`apt_section`       | The section for debian repos, e.g. `main` or `development`.    |
|`yum_beta`          | `"yes"` or `"no"`. Whether to enable the beta yum repo or not. |
|`ums_key`           | The license key for ANCWMC.                                       |
|`ancwmc_major_ver`     | The major version of the repository, e.g. `5`.                 |
|`ums_major_ver`     | The major version of the repository, e.g. `5`.                 |

Example Playbook
----------------

```
#!yaml
- name: Install 
  hosts: my_server
  remote_user: ansible
  become: yes
  vars:
    repo_type: public
    apt_section: main
    yum_beta: "no"
    ums_key: 05X-UMS001-1111-1111-1111-1111
    ancwmc_major_ver: 5
    ums_major_ver: 5
  roles:
      - ancwmc
```

Author Information
------------------
Jennifer Yang <jennifer.yang@incognito.com>
