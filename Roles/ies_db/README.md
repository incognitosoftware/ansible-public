ies_db
=========

Installs either PostgreSQL or MySQL on the server and creates a database for IES.


Official OS Support 
-------------------

 - CentOS 7
 - Debian 8


Role Variables
--------------

| Variable                 | Default       | Description  |
| ------------------------ |-------------- | ------------ |
| `ies_db_type`            | `mysql`       | `mysql` or `postgres`. The database type for IES. |
| `ies_db_name`            | `ies`         | The database name for IES. |


Dependencies
------------

**Required for MySQL:**

| Variable                 | Description  |
| ------------------------ |------------- |
| `mysql_user`             | The database user for IES.     |
| `mysql_user_password`    | The user's password. |
| `mysql_root_password`    | The password for the root MySQL user. |
| `mysql_version`          | `5.5`, `5.6`, or `5.7`. The version of MySQL Server to install. |

**Required for PostgreSQL:**

| Variable                | Description  |
| ----------------------- | ------------ |
| `pg_management_user`    | The name of the management user to be created for the database. |
| `pg_management_password`| The password for the management user.                           |
| `postgresql_version`    | The version of PostgreSQL to install. See the `postgresql` role for OS availability. |
     			

Example Playbook
----------------

```
#!yaml
- name: Set up IES database
  hosts: my_server
  remote_user: ansible
  become: yes
  vars:
    ies_db_type: postgres
    pg_management_user: ansible
    pg_management_password: incognito
    postgresql_version: 9.1
  roles:
    - ies_db
```

Author Information
------------------
Support <support@incognito.com>
