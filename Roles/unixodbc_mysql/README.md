unixodbc_mysql
=========

Installs mysql-odbc connector and defines odbcinst.ini.

Official OS Support
------------

 - CentOS 7
 - Debian 8

Role Variables
--------------

None

Dependencies
------------

None

Example Playbook
----------------

```
#!yaml
- name: Install mysql-odbc connector
  hosts: my_server
  remote_user: ansible
  become: yes
  roles:
    - unixodbc_mysql
```

Author Information
------------------
Support <support@incognito.com>
