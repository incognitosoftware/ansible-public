Role Name
=========

acs_db: Installs either PostgreSQL or MySQL on the server and creates a database for ACS.


Official OS Support
-------------------

 - CentOS 7
 - Debian 8


Role Variables
--------------

| Variable                 | Default       | Description  |
| ------------------------ |-------------- | ------------ |
| `db_type`                | `mysql`       | `mysql` or `postgres`. The database type for ACS. |
| `acs_db_name`            | `acs`         | The database name for ACS. |


Dependencies
------------

**Required for MySQL:**

| Variable                 | Description  |
| ------------------------ |------------- |
| `mysql_user`             | The database user for ACS.     |
| `mysql_user_password`    | The user's password. |
| `mysql_root_password`    | The password for the root MySQL user. |
| `mysql_version`          | `5.5`, `5.6`, or `5.7`. The version of MySQL Server to install. |

**Required for PostgreSQL:**

| Variable                | Description  |
| ----------------------- | ------------ |
| `pg_management_user`    | The name of the management user to be created for the database. |
| `pg_management_password`| The password for the management user.                           |
| `postgresql_version`    | The version of PostgreSQL to install. See the `postgresql` role for OS availability. |


Example Playbook
----------------

```
#!yaml

- name: Set up ACS database
  hosts: someserver
  remote_user: ansible
  become: yes
  vars:
    db_type: mysql
    mysql_user: ansible
    mysql_user_password: secret
    mysql_root_password: secret
    mysql_version: 5.7
  roles:
    - acs_db

```

Author Information
------------------
Support <support@incognito.com>

