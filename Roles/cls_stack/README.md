Role Name
=========

cls\_stack : Installs the **latest** Central Lease Service and its GUI.


Official OS Support
-------------------

 - CentOS 7
 - Debian 8


Role Variables
--------------

None

Dependencies
------------

The following **MUST** be set in the playbook as vars :

| Variable           | Description  |
| ------------------ | ------------ |
|`repo_type`      | `Set as public` |
|`apt_section`  | `Type of repo. Typically this is set as "main".`|
|`yum_beta`   | `Should yum beta repository be enabled. Options are "no" or "yes"`|
|`cls_major_version` | The major version of the CLS repository, e.g. `2`.             |
|`cls_key`           | The license key for CLS. |

Example Playbook
----------------


```
#!yaml
- name: Install CLS stack
  hosts: my_server
  remote_user: ansible
  become: yes
  vars:
    repo_type: public
    apt_section: "main"
    yum_beta: "no"
    cls_major_version: 2
    cls_key: 02X-CLS002-1111-1111-1111-1111
  roles:
    - role: cls_stack 

```

Author Information
------------------
Support <support@incognito.com>
