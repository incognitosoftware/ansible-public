ies_gui
=========

Installs the IES GUI on the server, and configures the web.xml's. 


Official OS Support
------------

 - CentOS 7
 - Debian 8


Role Variables
--------------

| Variable                 | Default       | Description  |
| ------------------------ |-------------- | ------------ |
| `ies_gui_version`        | `None`        | Specific version to install, e.g. `1.1.1.17` |
| `ies_host`               | `default_ipv4.address`        | The location of the IES.     |
| `ums_host`               | `default_ipv4.address`   | The location of the UMS.          |


Dependencies
------------

| Variable                     | Description   |
| ---------------------------- |-------------- |
| `repo_type`                  | `Set as public`      |
| `apt_section`                | The section for debian repos, e.g. `main` or `development`. |
| `yum_beta`                   | `"yes"` or `"no"`. Whether to enable the yum beta repo or not. |
| `ies_major_ver`          | Specific version of IES to install, e.g. `2`. |
| `rabbitmq_user`      | The RabbitMQ user to be set in the web.xml.                    |
| `rabbitmq_password`  | The password for the RabbitMQ user to be set in the web.xml.   |


Example Playbook
----------------

```
#!yaml
- name: Install IES GUI
  hosts: my_server
  remote_user: ansible
  become: yes
  vars:
    repo_type: public
    apt_section: "main"
    yum_beta: "no"
    rabbitmq_user: incognito
    rabbitmq_password: incognito
    ies_major_ver: 2

  roles:
    - ies_gui 
```

Author Information
------------------
Support <support@incognito.com>

