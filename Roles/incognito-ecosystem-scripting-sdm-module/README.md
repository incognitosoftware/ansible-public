sdm\_svc
=========

Installs incognito-ecosystem-scripting-sdm-module on the server.

Official OS Support
------------

 - CentOS 7


Role Variables
--------------

| Variable                                            | Default       | Description  |
| ----------------------------------------------------|-------------- | ------------ |
| `ies_sdm_module_version`            | None          | Specific version of incognito-ecosystem-scripting-sdm-module to install, e.g. `5.10.1.3`. |

Dependencies
------------

The following **MUST** be set in the playbook as vars :

| Variable         | Description  |
| ---------------- | ------------ |
| `repo_type`      | `Options are local or public` |
| `apt_section`	  | `Type of repo`|
| `yum_beta`	   | `Should yum beta repository be enabled. Options are "no" or "yes"`|

Example Playbook
----------------

```
#!yaml
- name: Install incognito-ecosystem-scripting-sdm-module
  hosts: my_server
  remote_user: ansible
  become: yes

  vars:
    repo_type: "public"
    apt_section: "main"
    yum_beta: "no"
    
  roles:
    - incognito-ecosystem-scripting-sdm-module
```


Author Information
------------------
Jennifer Yang <jennifer.yang@incognito.com>
