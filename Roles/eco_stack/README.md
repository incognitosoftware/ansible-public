Role Name
=========

eco\_stack : Installs the Core Incognito Ecosystem services (CSS, CAS, LMS) and GUI's (CSS, CAS)


Official OS Support
-------------------

 - CentOS 7
 - Debian 8


Role Variables
--------------

None

Dependencies
------------

The following **MUST** be set in the playbook as vars :

| Variable           | Description  |
| ------------------ | ------------ |
|`repo_type`      | `Set as public` |
|`apt_section`    | `Type of repo. Typically this is set as "main".`|
|`yum_beta`       | `Should yum beta repository be enabled. Options are "no" or "yes"`|

Example Playbook
----------------

```
#!yaml
- name: Install Ecosystem
  hosts: my_server
  remote_user: ansible
  become: yes
  vars:
    repo_type: public
    apt_section: "main"
    yum_beta: "no"
  roles:
     - role: eco_stack 
```

Author Information
------------------
Support <support@incognito.com>
