Role Name
=========

openjdk\_runtime : Install openjdk on the server.

Official OS Support
-------------------


 - CentOS 7
 - Debian 8

Role Variables
--------------

| Variable          | Default       | Description  |
| ----------------- |-------------- | ------------ |
| `openjdk_version` | `7`           | Version of openjdk to install (See below for availability). |

OS Availability
--------------

| OS          | Available JRE versions |
| ----------- | -----------------------|
| Debian 8    | `7, 8`                 |
| CentOS 7    | `6, 7, 8`              |


Dependencies
------------

None


Example Playbook
----------------

```
#!yaml
- name: Installation of openjdk
  hosts: someserver
  remote_user: ansible
  become: yes 
  roles:
    - role: openjdk_runtime
```


Author Information
------------------

Support <support@incognito.com>
