ipms\_svc
=========

Install IPMS, license server and configure

Official OS Support
------------

 - CentOS 7

Role Variables
--------------

| Variable                 | Default       | Description                                                      |
| -----------------        |-------------- | ------------                                                     |
| `ipms_version`           | None          | Specific version of IPMS to install, e.g. `5.10.3.15`            |    
| `ipms_corba_port`        | `9993`        | The port for IPMS to listen on.                                  |
| `ipms_db_name`           | `ipms`        | Database name for IPMS.                                          |
| `ipms_db_type`           | `mysql`       | `mysql` or `postgres`. The database type for IPMS.               |
| `ipms_db_host`           | `eth0 IP`     | The location of the database.                                    |
| `ipms_postgresql_port`   | `5432`        | Port of PostgreSQL DB                                            |
| `ipms_db_port`           | `3306`        | Port of MySQL DB                                                 |
| `ipms_umscorba_port`     | `9995`        | Port of UMS                                                      |
| `install_db`     | `yes`   | 'yes' or 'no' - Install DB or not                          |


Dependencies
------------

The following **MUST** be set in the playbook as vars :

| Variable            | Description  |
| ------------------- | ------------ |
| `repo_type`         | `local` or `public`. The repository type to pull from.         |
| `yum_beta`          | `"yes"` or `"no"`. Whether to enable the beta yum repo or not. |
| `ipms_key`          | The license key for IPMS.                                      |
| `acnc_major_ver`    | The major version of the repository, e.g. `5`.                 |
| `odbc_database_type`| 'postgres' or 'mysql'. The database type for IPMS.    |


**Required for PostgreSQL:**

| Variable                | Description  |
| ----------------------- | ------------ |
| `pg_management_user`    | The name of the management user to be created for the database. |
| `pg_management_password`| The password for the management user.                           |
| `postgresql_version`    | The version of PostgreSQL to install. See the `postgresql` role for OS availability. |


**Required for MySQL:**

| Variable                 | Description  |
| ------------------------ |------------- |
| `mysql_user`          | Must be set as "system". The name of the MySQL user to be created. ONLY when MySQL DB is used |
| `mysql_user_password` | The password for the MySQL user that is created. ONLY when MySQL DB is used|


Example Playbook
----------------

**PostgreSQL:**

```
#!yaml
- name: Install IPMS
  hosts: my_server
  remote_user: ansible
  become: yes
  vars:
    repo_type: local
    apt_section: development
    yum_beta: "yes"
    acnc_major_ver: 5
    ipms_key: 05X-RCM300-1111-1111-1111-1111
    pg_management_user: incognito
    pg_management_password: secret
    ipms_db_type: postgres
    odbc_database_type: postgres
    postgresql_version: 9.5
  roles:
    - ipms_svc
```

Author Information
------------------
Jennifer Yang <jennifer.yang@incognito.com>
