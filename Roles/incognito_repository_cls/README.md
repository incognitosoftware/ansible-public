Role Name
=========

incognito\_repository\_cls :  Configure Incognito repository to allow CLS packages to be installed

Official OS Support
-------------------

 - CentOS 7
 - Debian 8

Role Variables
--------------

None

Dependencies
------------

The following **MUST** be set in the playbook as vars :

| Variable           | Description  |
| ------------------ | ------------ |
|`repo_type`      | `Set as public` |
|`apt_section`  | `Type of repo. Typically this is set as "main".`|
|`yum_beta`   | `Should yum beta repository be enabled. Options are "no" or "yes"`|
|`cls_key`           | The license key for CLS.                                       |
|`cls_major_version` | The major version of the repository, e.g. `2`.                 |


Example Playbook
----------------

```
#!yaml
- name: Setup CLS repo
  hosts: someserver
  remote_user: ansible
  become: yes
  vars:
    repo_type: public
    apt_section: "main"
    yum_beta: "no"
    cls_key: 02X-CLS001-1111-1111-1111-1111
    cls_major_version: 2
  roles:
    - role: incognito_repository_cls
```


Author Information
------------------

Support <support@incognito.com>
