ums_gui
=========

ums_gui : Installs UMS GUI on the server.

Official OS Support
------------

 - CentOS 6
 - Debian 7

Role Variables
--------------

| Variable             | Default        | Description  |
| -------------------- |--------------- | ------------ |
| `ums_host`           | `eth0 IP`    | The location of the UMS. |
| `ums_gui_version`    | None           | Specific version of the UMS GUI to install, e.g. `5.9.2.10`. |
	

Dependencies
------------

The following **MUST** be set in the playbook as vars :

| Variable           | Description  |
| ------------------ | ------------ |
|`repo_type`         | `Set as public`        |
|`apt_section`       | The section for debian repos, e.g. `main` or `development`.    |
|`yum_beta`          | `"yes"` or `"no"`. Whether to enable the beta yum repo or not. |
|`ums_key`           | The license key for UMS.                                       |
|`ums_major_ver`     | The major version of the repository, e.g. `5`.                 |

Example Playbook
----------------

```
#!yaml
- name: Install 
  hosts: my_server
  remote_user: ansible
  become: yes
  vars:
    repo_type: public
    apt_section: main
    yum_beta: "no"
    ums_key: 05X-UMS001-1111-1111-1111-1111
    ums_major_ver: 5
  roles:
      - ums_gui
```

Author Information
------------------
Support <support@incognito.com>

