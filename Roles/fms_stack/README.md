Role Name
=========

fms\_stack : Installs the **latest** Firmware Management Service and its GUI.


Official OS Support
-------------------

 - CentOS 7
 - Debian 8


Role Variables
--------------

None

Dependencies
------------

The following **MUST** be set in the playbook as vars :

| Variable           | Description  |
| ------------------ | ------------ |
|`repo_type`      | `Set as public` |
|`apt_section`  | `Type of repo. Typically this is set as "main".`|
|`yum_beta`   | `Should yum beta repository be enabled. Options are "no" or "yes"`|
|`fms_major_version` | The major version of the FMS repository, e.g. `2`.             |
|`fms_key`           | The license key for FMS. |

Example Playbook
----------------

```

#!yaml
- name: Install FMS and configure LSS
  hosts: someserver
  remote_user: ansible
  become: yes
  vars:
    repo_type: public
    apt_section: "main"
    yum_beta: "no"
    fms_major_version: 2
    fms_key: 02X-FMS001-1111-1111-1111-1111
  roles:
    - role: fms_stack

```

Author Information
------------------
Support <support@incognito.com>
