Role Name
=========

selinux :  Configures the SELinux mode and policy.

Official OS Support
-------------------

 - CentOS 7 + 6

Role Variables
--------------

| Variable                 | Default       | Description  |
| ------------------------ |-------------- | ------------ |
| `selinux_state` | `disabled`| `The SELinux mode either enforcing, permissive or disabled` |
| `selinux_policy` | `targeted`| `The SELinux policy to use either targeted, minimum or mls` |


Dependencies
------------

None


Example Playbook
----------------

```
#!yaml
- name: Setup of XYZ
  hosts: someserver
  remote_user: ansible
  become: yes
  vars:
    selinux_state: permissive
    selinux_policy: targeted
  roles:
     - role: selinux
```


Author Information
------------------

Support <support@incognito.com>
