Role Name
=========

cls : Installs Central Lease Service on the server, configures, and starts it.


Official OS Support
------------

The OS Support expected to work for this role

 - CentOS 7
 - Debian 8


Role Variables
--------------

**Note** : The latest version will be installed by default. Only when you do NOT want the latest do you need to define a specific version.


| Variable             | Default        | Description  |
| -------------------- |--------------- | ------------ |
| `cls_version`        | `None`         | Specific version of CLS to install, e.g `1.1.0.32`. |
| `db_host`            | `localhost`    | The database host for CLS        |
| `db_name`            | `cls`          | The database name for CLS        |
| `loglevel`           | `L1`           | loglevel for CLS. Possible values are: `"OFF", L1(MIN), L2, L3(MAX)`   |
| `flush`              | `"no"`         | `"yes"` or `"no"`. Whether or not to enable the FLUSH parameter in the CLS configuration file.        |
| `fraud_detection_interval` | `3600`   | The time in seconds between fraud detection tasks. Set to FRAUD_DETECTION_INTERVAL in the configuration file. |
| `smtp_relay_hostname`| `localhost`    | Hostname of the SMTP Relay for CLS to use. Set to SMTPRELAYHOSTNAME in the configuration file. |
| `smtp_email`         | `cls@localhost`| The email used for sending emails. Set to SMTPEMAIL in the configuration file. |
| `smtp_timeout`       | `20`           | Timeout for sending emails. set to SMTPTIMEOUT in the configuration file. |



Dependencies
------------

The following **MUST** be set in the playbook as vars :

| Variable           | Description  |
| ------------------ | ------------ |
|`repo_type`      | `Set as public` |
|`apt_section`  | `Type of repo. Typically this is set as "main".`|
|`yum_beta`   | `Should yum beta repository be enabled. Options are "no" or "yes"`|
|`cls_key`           | The license key for CLS.                                       |
|`cls_major_version` | The major version of the repository, e.g. `2`.                 |

Example Playbook
----------------

```
#!yaml
- name: Install CLS
  hosts: my_server
  remote_user: ansible
  become: yes
  vars:
    repo_type: public
    apt_section: "main"
    yum_beta: "no"
    cls_major_version: 2
    cls_key: 02X-CLS001-1111-1111-1111-1111
    roles:
      - role: cls
```

Author Information
------------------
Support <support@incognito.com>
