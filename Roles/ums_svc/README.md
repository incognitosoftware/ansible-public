ums\_svc
=========

Install UMS, license server and configure

Official OS Support
------------

 - CentOS 7 
 - Debian 8 

Role Variables
--------------

| Variable          | Default       | Description  |
| ----------------- |-------------- | ------------ |
| `ums_version`     | None          | Specific version of UMS to install, e.g. `5.9.2.4`            |    
| `ums_corba_port`  | `9995`        | The port for UMS to listen on.                          |
| `ums_db_name`	    | `ums`         | Database name for UMS.                                  |
| `ums_db_type`            | `mysql`       | `mysql` or `postgres`. The database type for UMS. |
| `ums_db_host`             | `eth0 IP`   | The location of the database.                           |
| `ums_postgresql_port`     | `5432`   | Port of PostgreSQL DB                          |
| `ums_mysql_port`     | `3306`   | Port of PostgreSQL DB                          |


Dependencies
------------

The following **MUST** be set in the playbook as vars :

| Variable            | Description  |
| ------------------- | ------------ |
| `repo_type`         | `Set as public`         |
| `apt_section`       | The section for debian repos, e.g. `main` or `development`.    |
| `yum_beta`          | `"yes"` or `"no"`. Whether to enable the beta yum repo or not. |
| `ums_key`           | The license key for UMS.                                       |
| `ums_major_ver`     | The major version of the repository, e.g. `5`.                 |

**Required for PostgreSQL:**

| Variable                | Description  |
| ----------------------- | ------------ |
| `pg_management_user`    | The name of the management user to be created for the database. |
| `pg_management_password`| The password for the management user.                           |
| `postgresql_version`    | The version of PostgreSQL to install. See the `postgresql` role for OS availability. |


**Required for MySQL:**

| Variable                 | Description  |
| ------------------------ |------------- |
| `mysql_user`          | Must be set as "system". The name of the MySQL user to be created. ONLY when MySQL DB is used |
| `mysql_user_password` | The password for the MySQL user that is created. ONLY when MySQL DB is used|


Example Playbook
----------------

**PostgreSQL:**

```
#!yaml
- name: Install UMS
  hosts: my_server
  remote_user: ansible
  become: yes
  vars:
    repo_type: public
    apt_section: main
    yum_beta: "no"
    ums_major_ver: 5
    ums_key: 05X-RCM100-1111-1111-1111-1111
    pg_management_user: incognito
    pg_management_password: secret
    ums_db_type: postgres
  roles:
    - ums_svc
```

Author Information
------------------
Support <support@incognito.com>
