sdm_gui
=========

Installs the SDM GUI on the server, and configures the web.xml's.


Official OS Support
------------

 - CentOS 6 + 7


Role Variables
--------------

| Variable                 | Default       | Description  |
| ------------------------ |-------------- | ------------ |
| `sdm_gui_version`        | `None`        | Specific version to install, e.g. `1.0.4.328` |
| `sdm_host`               | `default_ipv4.address`        | The location of the SDM.     |
| `ums_host`               | `default_ipv4.address`   | The location of the UMS.          |


Dependencies
------------

| Variable                     | Description   |
| ---------------------------- |-------------- |
| `repo_type`                  | `public` or `local`. The repository type to pull from.      |
| `apt_section`                | The section for debian repos, e.g. `main` or `development`. |
| `yum_beta`                   | `"yes"` or `"no"`. Whether to enable the yum beta repo or not. |
| `sdm_major_ver`          | Specific version of SDM to install, e.g. `1`. |
| `rabbitmq_user`      | The RabbitMQ user to be set in the web.xml.                    |
| `rabbitmq_password`  | The password for the RabbitMQ user to be set in the web.xml.   |
| `sdm_key`	   | `License key for SDM`|


Example Playbook
----------------

```
#!/usr/bin/env ansible-playbook
---

- name: Install sdm_gui_version GUI
  hosts: my_server
  remote_user: qa
  become: yes
  vars:
    repo_type: local
    apt_section: "development"
    yum_beta: "yes"
    rabbitmq_user: guest
    rabbitmq_password: guest
    sdm_major_ver: 1
    sdm_key: 1111-11XXX

  roles:
    - sdm_gui
```

Author Information
------------------
Antoaneta Dineva <adineva@incognito.com>
