Role Name
=========

incognito\_repository\_fms:  Configure Incognito repository to allow FMS packages to be installed

Official OS Support
-------------------

 - CentOS 7
 - Debian 8

Role Variables
--------------

None

Dependencies
------------

The following **MUST** be set in the playbook as vars :

| Variable           | Description  |
| ------------------ | ------------ |
|`repo_type`      | `Set as public` |
|`apt_section`  | `Type of repo. Typically this is set as "main".`|
|`yum_beta`   | `Should yum beta repository be enabled. Options are "no" or "yes"`|
|`fms_key`           | The license key for FMS.                                       |
|`fms_major_version` | The major version of the repository, e.g. `2`.                 |

Example Playbook
----------------

```
#!yaml
- name: Setup FMS repo
  hosts: someserver
  remote_user: ansible
  become: yes
  vars:
    repo_type: public
    apt_section: "main"
    yum_beta: "no"
    fms_key: 02X-FMS001-1111-1111-1111-1111
    fms_major_version: 2
  roles:
    - role: incognito_repository_fms
```


Author Information
------------------

Support <support@incognito.com>
