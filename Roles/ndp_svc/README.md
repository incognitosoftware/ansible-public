ndp\_svc
=========

Installs NDP on the server, configures, and starts it.

Official OS Support
------------

 - CentOS 7


Role Variables
--------------

| Variable                 | Default       | Description  |
| ------------------------ |-------------- | ------------ |
| `ndp_version`            | None          | Specific version of NDP to install, e.g. `1.3.6.10`. |
| `ndp_db_type`            | `mysql`       | `mysql` or `postgres`. The database type for NDP.       |
| `ndp_db_name`            | `ndp`         | The database name for NDP. |
| `ndp_db_host`            | `localhost`   | The location of the database.                           |
| `ndp_db_port`            | MySQL: `3306`, PostgreSQL: `5432` | The port used by the NDP database server. |
| `ndp_multicast_address`  | None          | `ip:port`. Value to be set for `MULTICAST_ADDRESS` in the NDP configuration file. |
| `ndp_multicast_region_id`| None          | `1-255`. Value to be set for `MULTICAST_NOTIFICATION_REGION_ID` in the NDP configuration file. |
| `ndp_repository_base_url`| `http://assets.incognito.com`    | Value to be set for `REPOSITORY_BASE_URL` in the NDP configuration file. |
| `ndp_ums_port`           | `9995`        | The port for UMS to listen on.                          |
| `ndp_ums_host`           | `default_ipv4.address`   | The location of the UMS. |
| `rabbitmq_port`          | `5672`        | The port of the RabbitMQ service. |  
| `LOGLEVEL`          | `L1`        | The level of the NDP logs. |


Dependencies
------------

The following **MUST** be set in the playbook as vars :

| Variable         | Description  |
| ---------------- | ------------ |
| `repo_type`      | `Options are local or public` |
| `apt_section`	  | `Type of repo`|
| `yum_beta`	   | `Should yum beta repository be enabled. Options are "no" or "yes"`|
| `ndp_key`	   | `License key for NDP`|
| `ndp_major_ver` | `Major version of repository e.g. 1` |
| `rabbitmq_user`          | The value for `MESSAGE_BROKER_USERNAME` to be set in the NDP config. |
| `rabbitmq_password`      | The value for `MESSAGE_BROKER_PASSWORD` to be set in the NDP config. |
| `rabbitmq_host`          | The address of the RabbitMQ server, e.g. `127.0.0.1`. Used in conjunction with `rabbitmq_port` to set MESSAGE_BROKER_ADDRESS in the NDP config. |

**Required for MySQL:**

| Variable                 | Description  |
| ------------------------ |------------- |
| `mysql_user`             | The database user for NDP.     |
| `mysql_user_password`    | The user's password. |

**Required for PostgreSQL:**

| Variable                | Description  |
| ----------------------- | ------------ |
| `pg_management_user`    | The name of the management user to be created for the database. |
| `pg_management_password`| The password for the management user.                           |

Example Playbook
----------------

```
#!yaml
- name: Install NDP
  hosts: my_server
  remote_user: ansible
  become: yes
  vars:
    repo_type: "public"
    apt_section: "main"
    yum_beta: "no"
    ndp_key: 01X-NDP100-1111-1111-1111-1111
    ndp_major_ver: 1
    ndp_db_type: postgres
    ndp_db_host: 1.2.3.4

  roles:
    - ndp_svc
```


Author Information
------------------
Jennifer Yang <jennifer.yang@incognito.com>
