Role Name
=========

bcc\_stack :  Installs all of the **latest** core BCC services and CLI's (DHCP, MPS, CFM Proxy, CFM + DNS) and the JIMC, sets the administrator password and registers each service.

**Note** : This role covers the most common use case for BCC. For more options (such as specific product version numbers) refer to each individual role. These are detailed in the `meta/main.yml` file.

Official OS Support
-------------------

 - CentOS 7
 - Debian 8

Role Variables
--------------

None


Dependencies
------------

The following **MUST** be set in the playbook as vars :

| Variable         | Description  |
| ---------------- | ------------ |
|`repo_type`      | `Set as public` |
|`apt_section`  | `Type of repo. Typically this is set as "main".`|
|`yum_beta`   | `Should yum beta repository be enabled. Options are "no" or "yes"`|
|`dhcp_key`   | `License key for DHCP`|
|`cfm_key`   | `License key for CFM`|
|`cfmp_key`   | `License key for CFM Proxy`|
|`dns_key`   | `License key for DNS`|
|`mps_key`   | `License key for MPS`|
|`bcc_major_ver` | `Major version of repository e.g. 7` |
|`bcc_admin_password` | `Set the administrator account password. Note: Password must be between 5 and 31 characters and CANNOT be the default of "incognito"` |


Example Playbook
----------------

```
#!yaml
- name: Setup of BCC Stack from Public Example
  hosts: testvm
  remote_user: ansible
  become: yes
  vars:
    repo_type: public
    apt_section: "main"
    yum_beta: "no"
    bcc_major_ver: 7
    bcc_admin_password : ansible
    dhcp_key: 07X-IPC100-1111-1111-1111-1111
    cfm_key: 07X-FGN100-1111-1111-1111-1111
    cfmp_key: 07X-FGP100-1111-1111-1111-1111
    dns_key: 07X-DNC100-1111-1111-1111-1111
    mps_key: 07X-DPM001-1111-1111-1111-1111
  roles:
     - role: bcc_stack
```


Author Information
------------------

Support <support@incognito.com>
