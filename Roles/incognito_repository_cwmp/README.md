Role Name
=========

incognito\_repository\_cwmp :  Configure Incognito repository to allow cwmp packages to be installed

Official OS Support
-------------------

 - CentOS 7 + 6
 - Debian 8 + 7

Role Variables
--------------

None

Dependencies
------------

The following **MUST** be set in the playbook as vars :

| Variable         | Description  |
| ---------------- | ------------ |
|`repo_type`       | `Set as public` |
|`apt_section`     | `Type of repo. Usually it's "main" for ga and "development" for latest`|
|`yum_beta`        | `Should yum beta repository be enabled. Options are "yes" or "no"`|
|`cwmp_key`        | `License key for cwmp`|
|`cwmp_major_ver`  | `Major version of repository e.g. 3` |


Example Playbook
----------------

```
#!yaml
  - name: Setup of XYZ
    hosts: someserver
    remote_user: ansible
    become: yes
    vars:
      repo_type: public
      apt_section: "main"
      yum_beta: "no"
      cwmp_key: 03X-CWM001-1111-1111-1111-1111
      cwmp_major_ver: 3
    roles:
      - role: incognito_repository_cwmp
```


Author Information
------------------
Support <support@incognito.com>
