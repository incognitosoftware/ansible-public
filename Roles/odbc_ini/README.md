odbc_ini
=========

Adds an entry to the `/etc/odbc.ini` file.

Official OS Support
------------

 - CentOS 7
 - Debian 8

Role Variables
--------------

| Variable             | Default        | Description  |
| -------------------- |--------------- | ------------ |
| `odbc_description`   | `ODBC Driver`  | `Description`|
| `odbc_trace`         | `Off`          | `Trace`      |
| `odbc_trace_file`    | None       | `TraceFile`  | 
| `odbc_server`        | `localhost`    | PostgreSQL: `Servername`, MySQL: `SERVER` |
| `odbc_driver`        | dependent on _odbc_database_type_ - **postgres:** `PostgreSQL` on CentOS, `PostgreSQL Unicode` on Debian / **mysql:** `MySQL` | `Driver` |
| `odbc_port`          | dependent on _odbc_database_type_ - **postgres:** `5432` / **mysql:** `3306` | `Port` |
| `odbc_mysql_socket`  | CentOS: `/var/lib/mysql/mysql.sock` / Debian: `/var/run/mysqld/mysqld.sock` | `Socket`     |
| `odbc_mysql_read_timeout` | 10      | `READTIMEOUT`        |
| `odbc_mysql_write_timeout`| 10      | `WRITETIMEOUT`       |
| `odbc_mysql_option`       | 4194304      | `OPTION`             |
| `odbc_pg_read_only`  | `"No"`         | `ReadOnly`           |
| `odbc_pg_row_versioning` | `"No"`     | `RowVersioning`      |
| `odbc_pg_show_system_tables` | `"No"` | `ShowSystemTables`   |
| `odbc_pg_show_oid_column` | `"No"`    | `ShowOidColumn`      |
| `odbc_pg_fake_oid_index`  | `"No"`    | `FakeOidIndex`       |
| `odbc_pg_conn_settings`   | None      | `ConnSettings`       |


Dependencies
------------

| Variable             | Description  |
| -------------------- | ------------ |
| `odbc_database_type` | `postgres` or `mysql`. |
| `odbc_database_name` | The database name for the created ini file entry. |

**Required for MySQL:**

| Variable                 | Description  |
| ------------------------ |------------- |
| `mysql_user`             | The database user for some service.     |
| `mysql_user_password`    | The user's password. |

**Required for PostgreSQL:**

| Variable                | Description  |
| ----------------------- | ------------ |
| `pg_management_user`    | The name of the management user to be created for the database. |
| `pg_management_password`| The password for the management user.                           |


Example Playbook
----------------

```
#!yaml
- name: Configure odbc 
  hosts: my_server
  remote_user: ansible
  become: yes
  vars:
    odbc_database_type: mysql
    odbc_database_name: some_name
    mysql_user: incognito
    mysql_user_password: incognito
    odbc_description: "Entry for some service"
    odbc_mysql_write_timeout: 10
  roles:
    - odbc_ini
```

Author Information
------------------
Support <support@incognito.com>
