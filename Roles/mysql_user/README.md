mysql_user
=========

Adds (or removes) a MySQL user from a MySQL database. To check user accounts on the MySQL cli you can run :

```

# mysql -uroot --password=incognito
select user,host from mysql.user;
```

This can be used to add (or remove) multiple hosts e.g. adding 10 CWMP IP's to a MySQL user.

Official OS Support
------------

 - CentOS 6, 7
 - Debian 7, 8


Role Variables
--------------

| Variable              | Default                 | Description  |
| --------------------- | ----------------------- | ------------ |
| `mysql_host`          | `127.0.0.1`             | The location of the database. |
| `mysql_user_priv`     | `<db_name>.*:ALL,GRANT` | The privileges granted to the user. The format is: `db.table:priv1,priv2` |
| `mysql_user_state`    | `present`               | `present` or `absent`. Whether to create or remove the user. |
| `append_privs`        | `no`                    | `Whether privilege should be appended, "no" overwrites user's privilege`  |
| `mysql_host_username` | `localhost`             | The 'host' part of the MySQL username which can be a JSON formatted string to add multiple IP/hostnames. See example below |

Dependencies
------------

| Variable              | Description  |
| --------------------- | ------------ |
| `mysql_user`          | The name of the MySQL user to be created.            |
| `mysql_user_password` | The password for the MySQL user that is created.     |
| `mysql_root_password` | The password for the MySQL root user.                |
| `mysql_db_name`       | The name of the database the user is being added to. |


Example Playbook
----------------

Example to create a MySQL user with a single host for the MySQL user

```
#!yaml
- name: Add user to database
  hosts: my_server
  remote_user: ansible
  become: yes
  vars:
    mysql_host: 127.0.0.1
    mysql_host_username: 1.2.3.4
    mysql_user_state: present
    mysql_user: incognito
    mysql_user_password: incognito
    mysql_root_password: incognito
    mysql_db_name: acs
    mysql_user_priv: "*.*:ALL"
  roles:
    - mysql_user
```

Example to create a MySQL user with a JSON formatted string to add multiple hosts

```
#!yaml
- name: Add user to database
  hosts: my_server
  remote_user: ansible
  become: yes
  vars:
    mysql_host: 127.0.0.1
    mysql_host_username: [ "1.2.3.4","2.2.2.2" ]
    mysql_user_state: present
    mysql_user: incognito
    mysql_user_password: incognito
    mysql_root_password: incognito
    mysql_db_name: acs
    mysql_user_priv: "*.*:ALL"
  roles:
    - mysql_user
```

Author Information
------------------
Support <support@incognito.com>
