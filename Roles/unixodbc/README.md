Role Name
=========

unixodbc : Install unixODBC

Official OS Support
-------------------

 - CentOS 7 + 6
 - Debian 8 + 7

Role Variables
--------------

None

Dependencies
------------

None

Example Playbook
----------------

```
#!yaml
- name: Setup of XYZ
  hosts: someserver
  remote_user: ansible
  become: yes
  roles:
	- role: unixodbc
```


Author Information
------------------
Support <support@incognito.com>
