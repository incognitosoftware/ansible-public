rabbitmq
=========

Install and configure RabbitMQ, create a new user and give them proper permissions.

Official OS Support
------------

  - CentOS 7
  - Debian 8


Role Variables
--------------

| Variable             | Default       | Description  |
| -------------------- |-------------- | ------------ |
| `rabbitmq_version`   | `None`        | It will default to the latest unless aSpecific version of RabbitMQ is specified e.g `3.5.7-1` |


Dependencies
------------

| Variable             | Description  |
| -------------------- | ------------ |
| `rabbitmq_user`      | The name of the rabbitmq user to create. |
| `rabbitmq_password`  | The password for the rabbitmq user that is created. |


Example Playbook
----------------
```
#!yaml
- name: Install RabbitMQ
  hosts: my_server
  remote_user: ansible
  become: yes
  vars:
    rabbitmq_user: ansible
    rabbitmq_password: secret
    rabbitmq_version: 3.5.7-1
  roles:
    - rabbitmq
```


Author Information
------------------
Support <support@incognito.com>
