Role Name
=========

incognito\_repository\_ums :  Configure Incognito repository to allow UMS packages to be installed

Official OS Support
-------------------

- CentOS : 6 + 7
- Debian : 7 + 8

Role Variables
--------------

None

Dependencies
------------

The following **MUST** be set in the playbook as vars :

| Variable         | Description  |
| ---------------- | ------------ |
|`repo_type`      | `Options are local or public` |
|`apt_section`	  | `Type of repo`|
|`yum_beta`	   | `Should yum beta repository be enabled. Options are "no" or "yes"`|
|`ums_key`	   | `License key for UMS`|
|`ums_major_ver` | `Major version of repository e.g. 5` |


Example Playbook
----------------

```
#!yaml
- name: Setup of XYZ
  hosts: someserver
  remote_user: ansible
  become: yes
  vars:
    repo_type: public
    apt_section: "main"
    yum_beta: "no"
    ums_key: 05X-RCM100-1111-1111-1111-1111
    ums_major_ver: 5
    roles:
	  - role: incognito_repository_ums
```


Author Information
------------------
Andre Kostur <akostur@incognito.com>
