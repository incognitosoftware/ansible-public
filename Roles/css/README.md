Role Name
=========

css : Installs Central Server Service

Official OS Support
------------

The OS Support expected to work for this role

 - CentOS 7
 - Debian 8

Role Variables
--------------

**Note** : The latest version will be installed by default. Only when you do NOT want the latest do you need to define a specific version.

| Variable             | Default       | Description  |
| -------------------- |-------------- | ------------ |
| `css_version`        | `None`        | Specific version of CSS to install, e.g `1.1.0.35`. |
| `css_db_host`        | `localhost`   | The database host for CSS. |
| `css_db_name`        | `css`         | The database name for CSS. |
| `loglevel`       | `L1`         | loglevel for CSS. Possible values are: `"OFF", L1(MIN), L2, L3(MAX)`. |


Dependencies
------------

The following **MUST** be set in the playbook as vars :

| Variable           | Description  |
| ------------------ | ------------ |
|`repo_type`      | `Set as public` |
|`apt_section`    | `Type of repo. Typically this is set as "main".`|
|`yum_beta`       | `Should yum beta repository be enabled. Options are "no" or "yes"`|

Example Playbook
----------------

```
#!yaml
- name: Install CSS
  hosts: my_server
  remote_user: ansible
  become: yes
  vars:
    repo_type: public
    apt_section: "main"
    yum_beta: "no"
  roles:
     - css
```

Author Information
------------------
Support <support@incognito.com>
