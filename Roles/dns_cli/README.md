Role Name
=========

dns\_cli :  Install the DNS CLI

Official OS Support
-------------------

 - CentOS 7
 - Debian 8

Role Variables
--------------

**Note** : The latest version will be installed by default. Only when you do NOT want the latest do you need to define a specific version.

| Variable                 | Default       | Description  |
| ------------------------ |-------------- | ------------ |
| `dns_cli_version`      | `None`| `Specific version to install e.g. 6.6.1.11 or if there are more than one revision of the same version e.g. 6.5.3.1-2)` |


Dependencies
------------

The following **MUST** be set in the playbook as vars :

| Variable         | Description  |
| ---------------- | ------------ |
|`repo_type`      | `Set as public` |
|`apt_section` | `Type of repo. Typically this is set as "main".`|
|`yum_beta`   | `Should yum beta repository be enabled. Options are "no" or "yes"`|
|`dns_key`   | `License key for DNS`|
|`bcc_major_ver` | `Major version of repository e.g. 7` |


Example Playbook
----------------

```
#!yaml
- name: Setup of XYZ
  hosts: someserver
  remote_user: ansible
  become: yes
  vars:
    repo_type: public
    apt_section: "main"
    yum_beta: "no"
    dns_key: 07X-DNC100-1111-1111-1111-1111
    bcc_major_ver: 7
  roles:
     - role: dns_cli
```


Author Information
------------------

Support <support@incognito.com>
