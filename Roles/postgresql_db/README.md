postgresql_db
=========

Creates a new postgresql database.


Official OS Support
------------

 - CentOS 7
 - Debian 8


Role Variables
--------------

| Variable       | Default     | Description  |
| -------------- | ----------- | ------------ |
| `pg_host`      | `127.0.0.1` | The host of the PostgreSQL Server where the database will be created. |
| `pg_admin_user`| `postgres`  | The name of the admin user for the PostgreSQL server. |
| `pg_encoding`  | `UTF-8`     | The encoding for the database. |
| `pg_locale`    | `C`         | The locale to be used for the database. | 
| `pg_template`  | `template0` | The template used to create the database. |

Dependencies
------------

| Variable                | Description  |
| ----------------------- | ------------ |
| `pg_management_user`    | The name of the management user to be created for the database. |
| `pg_management_password`| The password for the management user.                           |
| `pg_db_name`            | The name of the database to create.                             |


Example Playbook
----------------

```
#!yaml
- name: Create postgres database
  hosts: someserver
  remote_user: ansible
  become: yes
  vars:
    pg_management_user: ansible
    pg_management_password: secret
    pg_db_name: somename
  roles:
    - postgresql_db
```

Author Information
------------------
Support <support@incognito.com>
