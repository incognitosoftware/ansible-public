Role Name
=========

incognito\_repository\_ismp :  Configure Incognito repository to allow ISMP packages to be installed

Official OS Support
-------------------

- CentOS : 6 + 7

Role Variables
--------------

None

Dependencies
------------

The following **MUST** be set in the playbook as vars :

| Variable         | Description  |
| ---------------- | ------------ |
|`repo_type`      | `Options are local or public` |
|`yum_beta`	   | `Should yum beta repository be enabled. Options are "no" or "yes"`|
|`ismp_key`	   | `License key for ISMP`|
|`acnc_major_ver` | `Major version of repository e.g. 5` |


Example Playbook
----------------

```
#!yaml
- name: Setup of XYZ
  hosts: someserver
  remote_user: ansible
  become: yes
  vars:
    repo_type: public
    yum_beta: "no"
    ismp_key: 05X-SMP100-1111-1111-1111-1111
    acnc_major_ver: 5
    roles:
	    - role: incognito_repository_ismp
```


Author Information
------------------

Liam Clark <lclark@incognito.com>
