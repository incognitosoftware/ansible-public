ismp\_svc
=========

Installs ISMP on the server, configures, and starts it.

Official OS Support
------------

 - CentOS 7


Role Variables
--------------

| Variable                 | Default       | Description  |
| ------------------------ |-------------- | ------------ |
| `ismp_version`            | None          | Specific version of ISMP to install, e.g. `5.10.1.5`. |      
| `ismp_ums_port`           | `9995`        | The port for UMS to listen on.                          |
| `ismp_ums_host`           | `default_ipv4.address`   | The location of the UMS. | 
| `ismp_multicast_address`           | ` `   | The IMSP Multicast address. | 


Dependencies
------------

The following **MUST** be set in the playbook as vars :

| Variable         | Description  |
| ---------------- | ------------ |
| `repo_type`      | `Options are local or public` |
| `apt_section`	  | `Type of repo`|
| `yum_beta`	   | `Should yum beta repository be enabled. Options are "no" or "yes"`|
| `ismp_key`	   | `License key for ISMP`|
| `acnc_major_ver` | `Major version of repository e.g. 5` |


Example Playbook
----------------

```
#!yaml
- name: Install ISMP
  hosts: my_server
  remote_user: ansible
  become: yes
  vars:
    repo_type: local
    apt_section: development
    acnc_major_ver: 5
    yum_beta: "yes"
    ismp_key: 05X-SMP100-1111-1111-1111-1111
    ismp_multicast_address: 229.254.112.40:16040
  roles:
	  - role: ismp_svc
```


Author Information
------------------
Antonaneta Dineva <adineva@incognito.com>

