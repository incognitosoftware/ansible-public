Role Name
=========

incognito\_repository\_sdm :  Configure Incognito repository to allow SDM packages to be installed

Official OS Support
-------------------

- CentOS : 7

Role Variables
--------------

None

Dependencies
------------

The following **MUST** be set in the playbook as vars :

| Variable         | Description  |
| ---------------- | ------------ |
|`repo_type`      | `Options are local or public` |
|`yum_beta`	   | `Should yum beta repository be enabled. Options are "no" or "yes"`|
|`sdm_key`	   | `License key for SDM`|
|`sdm_major_ver` | `Major version of repository e.g. 1` |


Example Playbook
----------------

```
#!yaml
- name: Setup of XYZ
  hosts: someserver
  remote_user: ansible
  become: yes
  vars:
    repo_type: public
    yum_beta: "no"
    sdm_key: 01X-SDM001-1111-1111-1111-1111
    sdm_major_ver: 1
  roles:
	- role: incognito_repository_sdm
```


Author Information
------------------

Jennifer Yang <jennifer.yang@incognito.com>
