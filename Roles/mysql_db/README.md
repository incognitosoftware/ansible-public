mysql_db
=========

Creates a MySQL Database on the server.

Official OS Support
------------

 - CentOS 6, 7
 - Debian 7, 8


Role Variables
--------------

| Variable         | Default             | Description  |
| ---------------- | ------------------- | ------------ |
| `mysql_host`     | `127.0.0.1`         | The host of the MySQL Server where the database will be created. |
| `mysql_collation`| `latin1_general_ci` | The default collation used for comparison. |
| `mysql_encoding` | `latin1`            | The encoding mode for the database.        |

Dependencies
------------

| Variable              | Description  |
| --------------------- | ------------ |
| `mysql_root_password` | The password for the MySQL root user.            |
| `mysql_db_name`       | The name of the database to create.              |


Example Playbook
----------------

```
#!yaml
- name: Create mysql database
  hosts: my_server
  remote_user: ansible
  become: yes
  vars:
    mysql_user: incognito
    mysql_password: abc1234
    mysql_db_name: acs
    mysql_db_host: 1.2.3.4
  roles:
    - mysql_db
```

Author Information
------------------
Support <support@incognito.com>
