Role Name
=========

cas : Installs the Central Authentication Service

Official OS Support
------------

The OS Support expected to work for this role

 - CentOS 7
 - Debian 8

Role Variables
--------------

**Note** : The latest version will be installed by default. Only when you do NOT want the latest do you need to define a specific version.

| Variable             | Default       | Description  |
| -------------------- |-------------- | ------------ |
| `cas_version`        | `None`        | Specific version of CAS to install, e.g `1.1.0.40`. |
| `cas_db_host`        | `localhost`   | The database host for CAS. |
| `cas_db_name`        | `cas`         | The database name for CAS. |
| `loglevel`       | `L1`         | loglevel for CAS. Possible values are: `"OFF", L1(MIN), L2, L3(MAX)`. |


Dependencies
------------

The following **MUST** be set in the playbook as vars :

| Variable           | Description  |
| ------------------ | ------------ |
|`repo_type`      | `Set as public` |
|`apt_section`  | `Type of repo. Typically this is set as "main".`|
|`yum_beta`   | `Should yum beta repository be enabled. Options are "no" or "yes"`|


Example Playbook
----------------

```
#!yaml
- name: Install CAS
  hosts: my_server
  remote_user: ansible
  become: yes
  vars:
    repo_type: public
    apt_section: "main"
    yum_beta: "no"
  roles:
     - cas 
```

Author Information
------------------

Support <support@incognito.com>
