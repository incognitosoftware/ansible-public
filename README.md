![](AnsiblePublicLogo.png)
#Incognito Software Systems Ansible Public Repository

This repository contains Ansible roles to rapidly deploy products from small to large scale systems.

#Ansible Controller Installation

**Important** :  The version of Ansible supported by Incognito is currently **2.1.4.0**.  Strongly recommended to not use higher versions for Incognito deployments, due to known issues in Ansible with higher versions than our supported level.

This install flow uses Python [virtualenvs](http://docs.python-guide.org/en/latest/dev/virtualenvs/) which is not required but it strongly recommended.

## OSX

Install [Xcode](https://developer.apple.com/xcode/) if not already installed.

Setup as follows :

	$ cd ~

	$ easy_install pip

	$ sudo -H pip install virtualenv

	$ virtualenv --system-site-packages ansible

	$ source ~/ansible/bin/activate

	$ pip install ansible==2.1.4.0

	$ ansible --version
	    ansible 2.1.4.0
	    config file =
	    configured module search path = Default w/o overrides

To quit from the Ansible virtualenv at any time simply type :

	$ deactivate

## CentOS 7

Setup as follows :

	$ cd ~

	$ sudo yum groupinstall "Development Tools"

	$ sudo yum install epel-release libffi libffi-devel python-devel openssl openssl-devel

	$ sudo yum install python2-pip

	$ sudo pip install virtualenv

	$ virtualenv --system-site-packages ansible

	$ source ~/ansible/bin/activate

	$ pip install ansible==2.1.4.0

	$ ansible --version
	    ansible 2.1.4.0
	    config file =
	    configured module search path = Default w/o overrides


## Debian 8

Setup as follows :

    $ cd ~

    $ sudo apt update

    $ sudo apt install build-essential libssl-dev libffi-dev python-dev python-pip

    $ sudo pip2 install virtualenv

    $ virtualenv --system-site-packages ansible

    $ source ~/ansible/bin/activate

    $ pip2 install ansible==2.1.4.0

    $ ansible --version
    ansible 2.1.4.0
    config file =
    configured module search path = Default w/o overrides


To quit from the Ansible virtualenv at any time simply type :

    $ deactivate

#Configuration

## Alias Setup

Create the default config file if it does not already exist

	$ touch ~/.ansible.cfg

Update either `~/.bashrc` or `~/.bash_aliases` :

	alias activate_ansible='source ~/ansible/bin/activate && ansible --version'

Then to use Ansible in a **new** terminal session run :

	$ activate_ansible

Which should be similar to the below (replace jsmith with your username)

	$ activate_ansible
	ansible 2.1.4.0
  	  config file = /home/jsmith/.ansible.cfg
  	  configured module search path = Default w/o overrides

## Clone Ansible Repository

**Note** : Install `git` using your package manager (e.g. apt on Debian, yum on CentOS and homebrew on OSX)

Clone the Ansible repository to your local machine

    $ mkdir ~/git/
    $ cd ~/git/
    $ git clone https://bitbucket.org/incognitosoftware/ansible-public.git

##Defaults

Create and edit the configuration file by using `vim ~/.ansible.cfg`:

```
#!ini
[defaults]
roles_path = ~/git/ansible-public/Roles/
inventory = ~/ansibleinventory/hosts
retry_files_enabled = False

[ssh_connection]
control_path = %(directory)s/%%h-%%r
```

The `roles_path` parameter assumes the ansible project will be cloned to `~/git/`. If you have a different location change as appropriate.

## Inventory of Hosts

Ansible uses a hosts file to define an inventory for the target systems to run playbooks.

Create a folder to host the inventory and the hosts file

```
$ mkdir ~/ansibleinventory
$ touch ~/ansibleinventory/hosts
```

Within `~/ansibleinventory/hosts` you can define one or many hosts as targets e.g.

**SSH Authentication Keys example** : Strongly recommended

```
#!ini
[bcc_clusters]
bcc1 ansible_host=172.20.3.162
bcc2 ansible_host=172.30.3.199
```

**SSH Username + Password example** : The ssh password to use and never store this variable in plain text; always use a vault. See [Variables and Vaults](https://docs.ansible.com/ansible/playbooks_best_practices.html#best-practices-for-variables-and-vaults)

```
#!ini
[bcc_clusters]
bcc1 ansible_host=172.20.3.162 ansible_ssh_pass=<mypassword>
bcc2 ansible_host=172.30.3.199 ansible_ssh_pass=<mypassword>
```

In the above examples one could define to run against a single host e.g. `bcc1` or `bcc2` or against all hosts as `bcc_clusters`.

If you are required to use a non-default public key (e.g. on AWS) one could define

```
#!ini
[aws]
aws-acs ansible_host=52.37.19.168 ansible_ssh_private_key_file=~/.ssh/id_rsa_amazon
```

There are lots of options which can be defined. Please refer to the [Ansible documentation](http://docs.ansible.com/ansible/) for further information

# Environment Checks

 - Ansible has a `ping` module that can be used to quickly check if it can successfully connect to the target e.g. for target 'bcc1'

```
#!bash
$ ansible -u <remote_user> bcc1 -m ping

bcc1  | SUCCESS => {
    "changed": false,
    "ping": "pong"
}
```
- Syntax
	- u : target user name
	- m : module

- Another very useful module is `setup` to show everything it knows about that target e.g. for target 'bcc1'
	- OS version
	- BIOS details
	- Linux kernel
	- IPs
	- CPUs
	- RAM
- Example : Show the OS details
```
#!bash
$ ansible -u ansible bcc1 -m setup | egrep "ansible_distribution|ansible_os"                                                                              
        "ansible_distribution": "Debian",
        "ansible_distribution_major_version": "8",
        "ansible_distribution_release": "jessie",
        "ansible_distribution_version": "8.4",
        "ansible_os_family": "Debian",
```



# Example Playbooks

Ansible playbooks are a YAML format text file. The Examples folder contains examples to install Incognito Products.

An Example playbook is :

```
#!yml
#!/usr/bin/env ansible-playbook
---

- name: Setup of BCC Stack from Public Example
  hosts: changeme
  remote_user: changeme
  become: yes
  vars:
    repo_type: public
    apt_section: "main"
    yum_beta: "no"
    bcc_major_ver: 7
    bcc_admin_password: changeme
    dhcp_key: changeme
    cfm_key: changeme
    cfmp_key: changeme
    dns_key: changeme
    mps_key: changeme
  roles:
     - role: bcc_stack

```

 - Syntax
	- `name` : Identifies playbook
	- `hosts` : The target(s) hosts defined in the `inventory` (see Configuration section)
	- `remote_user` : username on target.
	- `become` : sudo access for the above user
	- `role` : The role to run defined via `roles_path'  (see Configuration section)

 - To execute the playbook make the file executable then run e.g. :

```
#!bash
 $ chmod +x bcc_stack_public.yml
 $ ./bcc_stack_public.yml
```

# FAQ

**1.** How do I obtain Incognito product keys?

If you do not have a valid license key please contact Incognito Sales <sales@incognito.com>  

**2.** Which Operating Systems do you support for each role?

Each role has a Readme file which details the current supported Operating Systems

**3.** Can I install BCC 6.x using the Ansible role?

No, currently Ansible roles only support BCC 7.x  

**4.** Can I use Ansible 2.2.x?

Not currently but it is on the roadmap

**5.** I am getting the error "Aborting, target uses selinux but python bindings (libselinux-python) aren't installed!"

We provide a role called `selinux` that can be run to install the SELinux python package to resolve this error.

**6.** Can I deploy on Debian using only an apt-cacher server?

Not currently but it is on the roadmap

# Contact Information

For any questions on the Incognito Software Systems Ansible Public Repository please first check the FAQ. For outstanding questions contact Incognito Support <support@incognito.com>.  
